const async = require('async');
const getSize = require('get-folder-size');

const constants = require('./utils/backupConstants');
const backupUtil = require('./utils/backupUtil');

async.waterfall(
    [
        (cb) => {
            backupUtil.mongoBackup(cb);
        },
        (cb) => {
            getSize(constants.BACKUP_BASE_DIR, cb);
        },
        (_dirSize, cb) => {
            console.log(
                `Backup storage used: ${((_dirSize / constants.MAX_BACKUP_STORAGE_USE) * 100)
                    .toFixed(1)
                    .padStart(5, ' ')}% of ${Math.round(constants.MAX_BACKUP_STORAGE_USE / 1024 / 1024)}MB`
            );
            if (_dirSize > constants.MAX_BACKUP_STORAGE_USE) {
                backupUtil.trimBackupDirectory(_dirSize - constants.MAX_BACKUP_STORAGE_USE, cb);
            } else {
                cb();
            }
        },
    ],
    (err) => {
        if (err) {
            console.error(err);
            return;
        }
        console.log('Backup script finished successfully');
    }
);
