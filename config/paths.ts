import * as path from 'path';
export default {
    SYSTEM_LOGS: path.join(__dirname, '..', 'logs'),
    MIGRATION_STATE: path.join(__dirname, '..', 'migrationState.json'),
    EMAIL_RESOURCES: path.join(__dirname, '..', 'email_res'),
};
