const dayjs = require('dayjs');
const testUtils = require('../utils/tests');

require('../setup');

describe('Budgets', async function () {
    let authRes;
    before(async function () {
        authRes = await testUtils.authenticateRandomNewUser();

        // import some transactions
        // categorise
    });

    describe('No-data calls should not fail', function () {
        it('should succeed for budget list', async function () {
            const res = await testUtils.apiGet(authRes.access_token, {
                url: `/api/budget`,
            });
            expect(res.status).to.equal(200);
            expect(res.body.items).to.be.an('array');
        });

        it('should succeed to fetch current year summary', async function () {
            const res = await testUtils.apiGet(authRes.access_token, {
                url: `/api/budget/${dayjs().year()}/category_summary`,
            });
            expect(res.status).to.equal(200);
        });

        it('should succeed to fetch observed plot data', async function () {
            const from = dayjs().startOf('year').format('YYYY-MM-DD');
            const to = dayjs().format('YYYY-MM-DD');
            const fieldsQuery = encodeURIComponent(JSON.stringify(['planned', 'plannedAdjusted']));
            const res = await testUtils.apiGet(authRes.access_token, {
                url: `/api/charts/line/${from}/to/${to}?fields=${fieldsQuery}`,
            });
            expect(res.status).to.equal(200);
        });
    });

    // test the following routes:
    // TODO: add an override to create budgets in previous years (in CI only maybe ? or a boolean flag in the body ?)
    // - budget list / get / create
    // - budget entry list / create / update / delete
    // - basic sanity check of the plot data routes
    // - time-travel -> updating categories without breaking things
    // - /api/budget/:year/category_summary
    // - /api/budget/:year/category_summary/:category_id
});
