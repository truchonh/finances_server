const config = require('../testConfig.json');
const _ = require('lodash');
const crypto = require('crypto');
const superagent = require('superagent');
const testUtils = require('../utils/tests');

require('../setup');

describe('Authentication', function () {
    let authRes;

    before(async function () {
        authRes = await testUtils.authenticate();
    });

    it('should authenticate successfully', async function () {
        expect(authRes).to.have.property('access_token');
        expect(authRes).to.have.property('expires');
        expect(authRes).to.have.property('expires_in');
        expect(authRes).to.have.property('refresh_token');
        expect(authRes).to.have.property('device_token');
        expect(authRes.device_token).to.equal(config.CI_USER_DEVICE_TOKEN);
    });

    it('should not create duplicate devices after multiple authentication', async function () {
        await testUtils.authenticate();
        let activeSessionRes = await testUtils.authenticate();

        const res = await testUtils.apiGet(activeSessionRes.access_token, {
            url: '/api/user',
        });
        expect(res.status).to.equal(200);
        expect(res.body).to.have.property('devices');
        expect(res.body.devices).to.have.lengthOf(1);
    });

    it('should be able to create a user', async function () {
        const userId = crypto.randomBytes(16).toString('base64');
        const res = await superagent.post(process.env.API_HOSTNAME + '/auth/token').send({
            id_token: config.CI_USER_PASSWORD,
            user_id: userId,
            type: 'google',
        });
        expect(res.status).to.equal(200);
    });

    describe('Users', async function () {
        let testUser;

        before(async function () {
            testUser = await testUtils.authenticateRandomNewUser();
        });

        it('should be able get profile info', async function () {
            const res = await testUtils.apiGet(testUser.access_token, {
                url: '/api/user',
            });
            expect(res.status).to.equal(200);

            const user = res.body;
            expect(user.googleUserId).to.equal(testUser.userId);
            expect(user.level).to.equal('user');
            expect(user.apiAccess).to.be.an('array');
            expect(user.apiAccess).to.be.empty;
        });

        describe('Web api', async function () {
            it('should allow api key generation', async function () {
                const res = await testUtils.apiPost(testUser.access_token, {
                    url: '/api/user/api_access',
                });
                expect(res.status).to.equal(200);
                expect(res.body).to.have.property('apiKey');
                expect(res.body.items).to.be.an('array');
                expect(_.last(res.body.items)).to.have.property('apiKey');
                expect(_.last(res.body.items)).to.have.property('creationDate');
            });

            it('should be able to call a protected route as a web service', async function () {
                const res = await testUtils.apiPost(testUser.access_token, {
                    url: '/api/user/api_access',
                });
                expect(res.status).to.equal(200);

                const authRes = await superagent
                    .post(`${process.env.API_HOSTNAME}/service/token`)
                    .send({ api_key: res.body.apiKey });
                expect(authRes.status).to.equal(200);
                expect(authRes.body).to.have.property('access_token');

                const testRes = await testUtils.apiGet(authRes.body.access_token, {
                    url: '/api/user',
                });
                expect(authRes.status).to.equal(200);
            });
        });
    });
});
