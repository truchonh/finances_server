const _ = require('lodash');
const dayjs = require('dayjs');

module.exports.generateTransactions = function (daysFromNow = 31) {
    const startDate = dayjs().subtract(daysFromNow, 'day');
    const now = dayjs();
    const allTransactions = [];
    let datePointer = startDate.clone().startOf('month');
    do {
        allTransactions.push(...getStatement(datePointer));
        datePointer = datePointer.add(1, 'month');
    } while (datePointer.isBefore(now, 'month'));
    return allTransactions.filter((t) => startDate.valueOf() <= t.date.getTime() && t.date.getTime() < Date.now());
};

function getStatement(dayInMonth) {
    const yearAndMonth = `${dayInMonth.year()}-${(dayInMonth.month() + 1).toString().padStart(2, '0')}`;

    let monthly = _.flatten(
        [
            // revenu mensuel
            create(`Crédit d'impôt pour solidarité`, `GOUV. QUÉBEC`, 71.58, 1, yearAndMonth, [5]),
            createForEveryDayOfWeek(`Paie`, `BOREAL-INFORMATIONS STRATEGIQUE`, 500, 1, yearAndMonth, 4),
            // bouffe
            createForEveryDayOfWeek(`Achat`, `MICROBRASSERIE LA MEMP`, -25, 1.15, yearAndMonth, 4),
            createForEveryDayOfWeek(`Achat`, `SUBWAY MONDAY !`, -13, 1.1, yearAndMonth, 1),
            create(`Achat`, `MCDONALD'S #1337`, -11, 1.1, yearAndMonth, 2),
            create(`Achat`, `A&W 5536`, -13, 1.1, yearAndMonth, 1),
            createForEveryDayOfWeek(`Achat`, `TIM HORTONS #3740`, -2.2, 1, yearAndMonth, 5),
            createForEveryDayOfWeek(`Achat`, `PROVIGO SHERBROOKE #84`, -92, 1.3, yearAndMonth, 6),
            // pret auto
            createForEveryDayOfWeek(`Prêt`, `BANQUE DE MONTREAL`, -50, 1, yearAndMonth, 2),
            // factures mensuel
            create(`Achat`, `DOMAINE LE MONTAGNAIS`, -333, 1, yearAndMonth, [1]),
            create(`Paiement`, `Koodo Mobile`, -35, 1, yearAndMonth, [15]),
            create(`Paiement`, `FIZZ`, -44.36, 1, yearAndMonth, [7]),
            create(`Assurance`, `ASSURANCE GÉNÉRAL DESJARDINS`, -76.27, 1, yearAndMonth, [21]),
            create(null, `Blizzard Entertainment 194-99551380 CA`, -20.88, 1, yearAndMonth, [18]),
            create('Achat', `NETFLIX.COM 866-716-0414 ON`, -10.99, 1, yearAndMonth, [28]),
            // transport
            create(null, `COUCHETARD #563 SHERBROOKE QC`, -50, 1.15, yearAndMonth, 3),
            // autres
            dayInMonth.month() % 3 === 0
                ? create('Achat', `Amazon.ca*FHASHKASDJFH AMAZON.CA ON`, -40, 1.2, yearAndMonth, 1)
                : [],
            (dayInMonth.month() + 1) % 6 === 0
                ? create('Achat', `Pay-to-build-your-own-keyboard CO.`, -750, 1.1, yearAndMonth, 1)
                : [],
        ],
        true
    );

    // prettier-ignore
    switch(dayInMonth.month()) {
        case 0: return _.flatten([
            monthly,
            // Changement d'huile !
            create('Achat', 'UNIVERSITY GALT SERVICE #1', -75, 1.2, yearAndMonth, 1),
            create(`TPS`, `CANADA`, 120.53, 1, yearAndMonth, [5]),
        ], true);
        case 1: return _.flatten([
            monthly,
        ], true);
        case 2: return _.flatten([
            monthly,
            create(`Achat`, `VERTIGE ESCALADE INC`, -495, 1, yearAndMonth, [23]),
        ], true);
        case 3: return _.flatten([
            monthly,
            create(`TPS`, `CANADA`, 120.53, 1, yearAndMonth, [5]),
        ], true);
        case 4: return _.flatten([
            monthly,
            // Les pneux d'été ! (au 3 ans)
            (dayInMonth.year() % 3 === 0 ? create('Achat', 'CANADIAN TIRE #42', -500, 1.1, yearAndMonth, 1) : []),
            // Retour d'impot !
            create(`Remboursement d'impôt`, `CANADA`, 500, 1.1, yearAndMonth, 1),
        ], true);
        case 5: return _.flatten([
            monthly,
        ], true);
        case 6: return _.flatten([
            monthly,
            // Réparation d'auto random !
            ((dayInMonth.year() - 5) % 8 === 0 ? create('Achat', 'UNIVERSITY GALT SERVICE #2', -378, 1.5, yearAndMonth, 1) : []),
            create(`TPS`, `CANADA`, 120.53, 1, yearAndMonth, [5]),
        ], true);
        case 7: return _.flatten([
            monthly,
        ], true);
        case 8: return _.flatten([
            monthly,
        ], true);
        case 9: return _.flatten([
            monthly,
            create(`TPS`, `CANADA`, 120.53, 1, yearAndMonth, [5]),
        ], true);
        case 10: return _.flatten([
            monthly,
            // Les pneux d'hiver ! (au 5 ans)
            ((dayInMonth.year() + 1) % 5 === 0 ? create('Achat', 'CANADIAN TIRE #42', -500, 1.1, yearAndMonth, 1) : []),
            create(`Achat`, `CORP.SKI & GOLF MONT-ORFORD`, -143.74, 1, yearAndMonth, [25]),
        ], true);
        case 11: return _.flatten([
            monthly,
            // Réparation d'auto random !
            ((dayInMonth.year() + 2) % 3 === 0 ? create('Achat', 'UNIVERSITY GALT SERVICE #3', -251, 1.3, yearAndMonth, 1) : []),
        ], true);
    }
}

function createForEveryDayOfWeek(type, description, amount, variance, yearAndMonth, dayIndex) {
    let dateSplit = yearAndMonth.split('-');
    let daysInMonth = new Date(dateSplit[0], dateSplit[1], 0).getDate();
    let date = new Date(dateSplit[0], dateSplit[1] + 1, 1);
    let days = [];
    for (let i = 1; i <= daysInMonth; i++) {
        date.setDate(i);
        if (dayIndex === date.getDay()) {
            days.push(i);
        }
    }
    return create(type, description, amount, variance, yearAndMonth, days);
}

function create(type, description, amount, variance, yearAndMonth, days) {
    // Add ascii code values of all characters together to form a "random" number
    let random =
        description.split('').reduce((acc, char) => (acc += Math.max(100, char.charCodeAt(0))), 0) *
        new Date().getFullYear();
    let varianceRange = Math.round(Math.abs(amount) * (variance - 1) * 100) / 100; // round to exact penny values
    let getAmount = function () {
        let modifier = varianceRange !== 0 ? random % (varianceRange * 2) : 0;
        // modify the "random" value to get a new one each time !
        random += (1 / modifier) * new Date().getFullYear();
        const _amount = Math.abs(amount) + modifier - varianceRange;
        return Math.round(_amount * 100) / 100;
    };

    if (typeof days === 'number') {
        // 'days' is the number of pseudo random days to generate.
        let dateSplit = yearAndMonth.split('-');
        let daysInMonth = new Date(dateSplit[0], dateSplit[1], 0).getDate();

        let dayCount = days;
        days = [];
        for (let i = 0; i < dayCount; i++) {
            let day = random % daysInMonth;
            random += (1 / Math.max(1, day)) * new Date().getFullYear();
            days.push(day + 1);
        }
    } else {
        // 'days' is the exact list of days to create transactions for.
        // Do nothing.
    }

    return days.map((day) => ({
        date: dayjs(`${yearAndMonth}-${day.toString().padStart(2, '0')}`, 'YYYY-MM-DD').toDate(),
        description: type ? `${type} /${description}` : description,
        amount: getAmount() * (amount >= 0 ? 1 : -1),
        account: 'demo_data',
        currency: 'CAD',
    }));
}
