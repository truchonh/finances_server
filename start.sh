#!/bin/bash

# stop running containers
docker-compose -f production.yml stop pdf mailer api

# update the containers
docker-compose -f production.yml pull
docker-compose -f production.yml up --build -d pdf mailer api

#copy the secrets (ths may fail if the container names changes)
docker cp secrets.json finances_api_mailer_1:/lib/mailer/secrets.json
docker cp secrets.json finances_api_api_1:/lib/finances_server/secrets.json