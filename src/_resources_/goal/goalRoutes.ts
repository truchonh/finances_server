import * as _ from 'lodash';
import * as mongoose from 'mongoose';
import * as dayjs from 'dayjs';
import { AuthenticatedRequest, makeAuthenticatedRouter, makeRequestHandler } from '../../utils/queryUtil';
import { defaultContext } from '../../utils/context';
import { SystemCategory, CategoryType } from '../../models/enums';
import { ClientError } from '../../utils/errors';
import * as dateUtil from '../../utils/dateUtil';

import { Goal } from '../../data/goalCollection';
import { Category } from '../../data/categoryCollection';
import { Budget } from '../../data/budgetCollection';
import { getCategories } from '../budget/budgetController';
import { Transaction } from '../../data/transactionCollection';

// prettier-ignore
export default function getGoalRouter() {
    const router = makeAuthenticatedRouter();

    router.route('/:goal_id')
        .all(defaultContext)
        .put(makeRequestHandler(update))
        .delete(makeRequestHandler(_delete));
    router.route('/:goal_id/complete')
        .all(defaultContext)
        .put(makeRequestHandler(complete));
    router.route('/')
        .get(makeRequestHandler(listGoals))
        .post(makeRequestHandler(create));

    return router;
}

async function complete(req: AuthenticatedRequest) {
    const goal = req.context.goal_id;
    const transactionIds =
        req.body.transactions?.map((id: mongoose.Types.ObjectId) => new mongoose.Types.ObjectId(id)) || [];
    if (_.isEmpty(transactionIds)) {
        throw new ClientError('Au moins une transaction doit être sélectionné.');
    }

    const { category: goalCategory } = await getGlobalGoalBudget(req.userId);
    await Category.createIfNotExistsOrUpdate(
        req.userId,
        CategoryType.base,
        goal.name,
        SystemCategory.expense,
        [],
        goalCategory.name
    );

    const transactions = await Transaction.find({ _id: transactionIds }).forUser(req.userId);
    for (let transaction of transactions) {
        transaction.category = goal.name;
        transaction.ignoreForAutoCategoryAssignment = true;
        await transaction.save();
    }

    goal.completionDate = new Date();
    goal.finalAmount = Math.abs(_.sumBy(transactions, (t) => t.amount));
    goal.transactions = transactionIds;
    await goal.save();
}

async function update(req: AuthenticatedRequest) {
    const goal = req.context.goal_id;
    const isCompleted = !!goal.completionDate;
    if (isCompleted) {
        throw new ClientError('Impossible de modifier un objectif complété.');
    }

    const updatedGoal = _.pick(req.body, ['name', 'amount', 'description', 'deadline', 'necessity']);
    goal.name = updatedGoal.name;
    goal.amount = updatedGoal.amount;
    goal.description = updatedGoal.description;
    goal.deadline = updatedGoal.deadline;
    goal.necessity = updatedGoal.necessity;
    await goal.save();

    return goal.toDto();
}

async function _delete(req: AuthenticatedRequest) {
    const goal = req.context.goal_id;
    const isCompleted = !!goal.completionDate;
    if (isCompleted) {
        throw new ClientError('Impossible de supprimer un objectif complété.');
    }
    await goal.remove();
}

async function listGoals(req: AuthenticatedRequest) {
    const goals = await Goal.find({
        $or: [
            { completionDate: { $exists: false } },
            { completionDate: null },
            {
                completionDate: {
                    $gte: dayjs().startOf('year').toDate(),
                    $lte: dayjs().endOf('year').toDate(),
                },
            },
        ],
    }).forUser(req.userId);
    const { budgetEntry: goalBudgetEntry } = await getGlobalGoalBudget(req.userId);
    return {
        // all goals completed that year + all pending goals
        items: goals.map((goal) => goal.toDto()),
        // infos about the goals summary
        goalYearlyBudget: goalBudgetEntry ? dateUtil.periodsInYear(goalBudgetEntry.period) * goalBudgetEntry.amount : 0,
        goalCarryover: await calculateCarryoverGoalBudget(req.userId),
    };
}

async function getGlobalGoalBudget(userId: string) {
    const budget = await Budget.getClosest(userId, dayjs().year(), true);
    if (!budget || budget.year !== dayjs().year()) {
        throw new ClientError(`Aucun budget d'ouvert pour l'année courante.`);
    }
    const goalCategory = (await getCategories(budget)).find((category) => category.systemType === SystemCategory.goal);
    const goalBudgetEntry = goalCategory && budget.entries.find((entry) => entry.categoryId.equals(goalCategory._id));
    return {
        category: goalCategory,
        budgetEntry: goalBudgetEntry,
    };
}

async function calculateCarryoverGoalBudget(userId: string) {
    let goalBudgetSum = 0;
    const completedGoals = await Goal.find({
        completionDate: { $exists: true, $lt: dayjs().startOf('year').toDate() },
    }).forUser(userId);
    const budgets = await Budget.find({ userId });
    for (let budget of budgets) {
        const goalCategory = budget.categoriesSnapshot.find((c) => c.systemType === SystemCategory.goal);
        const goalBudgetEntry =
            goalCategory && budget.entries.find((entry) => entry.categoryId.equals(goalCategory._id));
        if (goalBudgetEntry) {
            const goalYearlyBudget = dateUtil.periodsInYear(goalBudgetEntry.period) * goalBudgetEntry.amount;
            goalBudgetSum += goalYearlyBudget;
        }
    }
    return goalBudgetSum - _.sumBy(completedGoals, (goal) => goal.finalAmount);
}

async function create(req: AuthenticatedRequest) {
    // pretend validation is done already
    const goal = new Goal({
        ..._.pick(req.body, ['name', 'amount', 'description', 'deadline', 'necessity']),
        userId: req.userId,
    });
    await goal.save();
    return goal.toDto();
}
