import { AuthenticatedRequest, makeAuthenticatedRouter, makeRequestHandler } from '../../utils/queryUtil';
import { Response } from 'express';
import * as _ from 'lodash';
import * as errorHandler from '../../utils/errorHandler';
import * as FileDao from '../../data/fileCollection';
import { ClientError } from '../../utils/errors';
import { getStream } from '../../utils/multipartUtil';
import { defaultContext } from '../../utils/context';
import { LeanStatement, Statement } from '../../data/statementCollection';
import { Transaction } from '../../data/transactionCollection';
import { AttachmentType } from '../../models/enums';

const Readable = require('stream').Readable;

export default function getAttachmentRouter() {
    const router = makeAuthenticatedRouter();

    router
        .route('/:attachment_id')
        .get((req: AuthenticatedRequest, res) => download(req, res))
        .delete(makeRequestHandler(delete_));

    return router;
}

export function getAttachmentTransactionRouter() {
    // The base route of this router is /api/transaction/
    const router = makeAuthenticatedRouter();

    router
        .route('/:transaction_id/attachment')
        .all(defaultContext)
        .post(makeRequestHandler(uploadTransactionAttachment));
    router
        .route('/:transaction_id/attachment/:attachment_id')
        .all(defaultContext)
        .delete(makeRequestHandler(deleteTransactionAttachment));

    return router;
}

async function download(req: AuthenticatedRequest, res: Response) {
    let params = req.params;

    try {
        const file = await FileDao.findOne({ _id: params.attachment_id });
        if (!file) {
            return errorHandler.handle404(
                req,
                res,
                new Error(`Aucune pièce jointe avec l'ID ${params.attachment_id}.`)
            );
        }

        // check the owner of the statement / transaction this file is attached to
        // TODO: This is a bad way to secure the file
        const statement = await Statement.findOne({
            fileId: params.attachment_id,
        })
            .forUser<LeanStatement>(req.userId)
            .lean();
        const transaction = await Transaction.findOne({
            'attachments.fileId': params.attachment_id,
        })
            .forUser(req.userId)
            .lean();
        if (!statement && !transaction) {
            return errorHandler.handle403(req, res, new Error(`Vous n'avez pas accès à ce fichier.`));
        }

        const stream = await FileDao.download(params.attachment_id);
        res.attachment(file.filename);
        stream.pipe(res);
    } catch (err) {
        errorHandler.handle500(req, res, err);
    }
}

async function delete_(): Promise<void> {
    // TODO: this was never implemented ???
    throw new Error('attachment delete not implemented');
}

async function deleteTransactionAttachment(req: AuthenticatedRequest) {
    let params = req.params;
    const transaction = req.context.transaction_id;

    transaction.attachments = transaction.attachments.filter(
        (_attachment) => _attachment.fileId !== params.attachment_id
    );
    transaction.markModified('attachments');
    await transaction.save();

    await FileDao.remove(params.attachment_id);
}

async function uploadTransactionAttachment(req: AuthenticatedRequest) {
    const transaction = req.context.transaction_id;
    let body = req.body;

    let multipartData;
    if (_.isEmpty(body)) {
        multipartData = await getStream(req);
    }

    if (!_.isEmpty(body)) {
        if (body.eml) {
            const stream = Readable.from(new Buffer(body.eml, 'base64').toString('ascii'));
            return await transaction.attachFile(stream, AttachmentType.eml, body.filename);
        } else {
            const stream = Readable.from(JSON.stringify(body));
            return await transaction.attachFile(stream, AttachmentType.json, 'data.json');
        }
    } else if (multipartData) {
        return await transaction.attachFile(multipartData.stream, AttachmentType.file, multipartData.filename);
    } else {
        throw new ClientError(`Aucun fichier n'a été reçu.`);
    }
}
