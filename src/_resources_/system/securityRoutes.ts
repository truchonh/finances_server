import * as _ from 'lodash';
import { OAuth2Client } from 'google-auth-library';
import { Request, Response, NextFunction } from 'express';
import * as errorHandler from '../../utils/errorHandler';
import * as secretUtils from '../../utils/secretUtils';
import { HydratedUser, IUserLocation, User } from '../../data/userCollection';
import { prepareResponse, AuthenticatedRequest, PendingAuthenticationRequest } from '../../utils/queryUtil';
import { doLocationSecurityCheck, doServiceLocationSecurityCheck } from './securityController';
import { lookup } from 'geoip-lite';
import { toMs } from 'ms-typescript';
const ipware = require('ipware');

const getIp = ipware().get_ip as (request: Request) => { clientIp: string; clientIpRoutable: boolean };

const CLIENT_ID = '608076979786-5clg6tlbpaeefkpv3ce43h93eh6se53f.apps.googleusercontent.com';
const oAuthClient = new OAuth2Client(CLIENT_ID);

const token_ttl = '10m';
const refresh_ttl = '31d';

export function getAuthenticate() {
    let middleware = [];

    middleware.push(setLocationContextMiddleware);
    middleware.push(verifyUser);
    middleware.push(locationSecurityMiddleware);
    middleware.push(generateTokens);

    return middleware;
}

export function getServiceAuthenticate() {
    let middleware = [];

    middleware.push(setLocationContextMiddleware);
    middleware.push(verifyApiKey);
    middleware.push(serviceLocationSecurityMiddleware);
    middleware.push(generateTokens);

    return middleware;
}

export function deviceAuthorisationMiddleware() {
    return [setLocationContextMiddleware, authorizeDevice];
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/// Authentication related private methods
///
function setLocationContextMiddleware(req: PendingAuthenticationRequest, _res: Response, next: NextFunction) {
    const ip = getIp(req).clientIp;
    const location: IUserLocation = _.pick(lookup(ip), ['country', 'region', 'city', 'll']);
    req.locationContext = {
        device: req.body.device || {},
        ip,
        location,
        locationLabel: [location.city, location.region, location.country].filter((_token) => _token).join(', '),
    };
    next();
}

async function verifyUser(req: PendingAuthenticationRequest, res: Response, next: NextFunction) {
    if (req.body.type === 'google') {
        await verifyGoogleAuthToken(req, res, next);
    } else if (req.body.type === 'demo') {
        await verifyDemoUserAccess(req, res, next);
    } else {
        res.status(400).send({
            message: 'Authentication type missing.',
        });
    }
}

async function verifyGoogleAuthToken(req: PendingAuthenticationRequest, res: Response, next: NextFunction) {
    if (process.env.NODE_ENV === 'DEV' || process.env.NODE_ENV === 'CI') {
        if (req.body.id_token === 'password1') {
            // FIXME:             Super bad (and convenient) backdoor, please fix me before release.
            // FIXME:        ____    __    ____  ___      .______      .__   __.  __  .__   __.   _______
            // FIXME:        \   \  /  \  /   / /   \     |   _  \     |  \ |  | |  | |  \ |  |  /  _____|
            // FIXME:         \   \/    \/   / /  ^  \    |  |_)  |    |   \|  | |  | |   \|  | |  |  __
            // FIXME:          \            / /  /_\  \   |      /     |  . `  | |  | |  . `  | |  | |_ |
            // FIXME:           \    /\    / /  _____  \  |  |\  \----.|  |\   | |  | |  |\   | |  |__| |
            // FIXME:            \__/  \__/ /__/     \__\ | _| `._____||__| \__| |__| |__| \__|  \______|

            let user = await User.findOne<HydratedUser>({ googleUserId: req.body.user_id });
            if (!user) {
                user = await User.createIfNotExist({
                    sub: req.body.user_id,
                    given_name: req.body.user_id,
                });
            }
            req.user = user;
            req.userId = user.googleUserId;
        }
        return next();
    }

    /**
     * @property id_token {string}
     * @property user_id {string}
     */
    let body = req.body;

    try {
        let user;

        const ticket = await oAuthClient.verifyIdToken({
            idToken: body.id_token,
            audience: CLIENT_ID,
        });
        const payload = ticket.getPayload();

        // Check if the user id is matching
        if (body.user_id !== payload['sub']) {
            return errorHandler.handle4XX(req, res, `Impossible d'authentifier l'usager.`, 403, true);
        }

        user = await User.createIfNotExist(payload);

        req.user = user;
        req.userId = body.user_id;

        next();
    } catch (err) {
        // failed to verify the user's token
        return errorHandler.handle4XX(req, res, `Impossible d'authentifier l'usager.`, 403, true);
    }
}

async function verifyDemoUserAccess(req: PendingAuthenticationRequest, res: Response, next: NextFunction) {
    const authHeader = req.headers.authorization;
    if (!authHeader) {
        return errorHandler.handle401(req, res, 'No token provided');
    }

    const token = authHeader.split(' ')[1];

    const payload = secretUtils.verify<{ deviceId: string }>(token);
    if (payload === false) {
        let verifyError = secretUtils.verifyErr(token);
        return errorHandler.handle401(req, res, `Invalid token\nVerification error: ${verifyError}`);
    } else {
        req.user = await User.findOne({ googleUserId: 'demo_user' });
        req.userId = req.user.googleUserId;
        req.deviceId = payload['deviceId'];

        next();
    }
}

async function verifyApiKey(req: PendingAuthenticationRequest, res: Response, next: NextFunction) {
    const hashedKey = secretUtils.tryHashApiKey(req.body.api_key);

    try {
        let user = await User.findOne({
            apiAccess: {
                $exists: true,
            },
            'apiAccess.apiKey': hashedKey,
        });
        if (!user) {
            return errorHandler.handle4XX(req, res, `Clé d'api invalide.`, 403, true);
        }

        const apiAccess = user.apiAccess.find((_access) => _access.apiKey === hashedKey);
        if (apiAccess.disabled) {
            return errorHandler.handle4XX(req, res, `Clé d'api expiré.`, 403, true);
        }

        apiAccess.lastActivity = new Date();
        user.markModified('apiAccess');
        await user.save();

        req.user = user;
        req.userId = user.googleUserId;
        req.hashedApiKey = hashedKey;

        next();
    } catch (err) {
        return errorHandler.handle4XX(req, res, `Impossible d'authentifier cette clé d'api.`, 403, true);
    }
}

async function locationSecurityMiddleware(req: PendingAuthenticationRequest, res: Response, next: NextFunction) {
    await doLocationSecurityCheck({
        locationContext: req.locationContext,
        user: req.user,
    });
    next();
}

async function serviceLocationSecurityMiddleware(req: PendingAuthenticationRequest, res: Response, next: NextFunction) {
    await doServiceLocationSecurityCheck({
        locationContext: req.locationContext,
        user: req.user,
        hashedKey: req.hashedApiKey,
    });
    next();
}

async function generateTokens(req: PendingAuthenticationRequest, res: Response) {
    const user = req.user;
    const device = req.locationContext.device;
    let existingDevice = user.getDevice(device.deviceToken || req.hashedApiKey);

    req.deviceId = existingDevice?._id?.toString();
    let response: any = {
        device_token: device.deviceToken,
    };

    if (existingDevice?.verified) {
        response = {
            ...response,
            access_token: secretUtils.sign({ sub: req.userId, deviceId: req.deviceId }, token_ttl),
            expires: Date.now() + toMs(token_ttl),
            expires_in: toMs(token_ttl),
            refresh_token: existingDevice && secretUtils.sign({ sub: req.userId }, refresh_ttl),
        };
    }

    if (process.env.NODE_ENV !== 'CI' && existingDevice && response.refresh_token) {
        existingDevice.refreshToken = response.refresh_token;
        user.markModified('devices');
        await user.save();
    }

    return res.send(await prepareResponse(req, response));
}

export async function refreshToken(req: AuthenticatedRequest, res: Response) {
    /**
     * @property refresh_token {string}
     */
    const body = req.body;

    let payload = secretUtils.verify<{ sub: string }>(body.refresh_token);
    if (payload !== false) {
        let user = await User.findOne({ googleUserId: payload['sub'] });
        req.user = user;
        req.userId = user.googleUserId;

        // TODO: Find some way to prevent the CI from crashing somewhere here
        const device = user.devices.find((_device) => _device.refreshToken === body.refresh_token);
        if (!device) {
            return errorHandler.handle401(req, res, 'Invalid refresh token');
        }

        req.deviceId = device._id.toString();
        let response = {
            access_token: secretUtils.sign({ sub: payload['sub'], deviceId: req.deviceId }, token_ttl),
            expires: Date.now() + toMs(token_ttl),
            expires_in: toMs(token_ttl),
            // rotate the refresh token to reduce attack risk
            refresh_token: secretUtils.sign({ sub: payload['sub'] }, refresh_ttl),
        };

        device.refreshToken = response.refresh_token;
        user.markModified('devices');
        await user.save();

        return res.send(await prepareResponse(req, response));
    } else {
        return errorHandler.handle401(req, res, 'Invalid refresh token');
    }
}

export async function verificationMiddleware(req: AuthenticatedRequest, res: Response, next: NextFunction) {
    let authHeader = req.headers.authorization;
    if (!authHeader) {
        // Authentication failed.
        return errorHandler.handle401(req, res, 'No token provided');
    } else {
        const token = authHeader.split(' ')[1];

        let payload = secretUtils.verify<{ sub: string; deviceId: string }>(token);
        if (payload === false) {
            // Token verification failed.
            let verifyError = secretUtils.verifyErr(token);
            return errorHandler.handle401(req, res, `Invalid token\nVerification error: ${verifyError}`);
        } else {
            // Token is valid. Attach the user id to the request object.
            req.userId = payload['sub'];
            req.deviceId = payload['deviceId'];
            req.user = await User.findOne({ googleUserId: payload['sub'] });
            if (req.user) {
                next();
            } else {
                errorHandler.handle500(req, res, new Error('User does not exists.'));
            }
        }
    }
}

export async function authorizeDevice(req: PendingAuthenticationRequest, res: Response) {
    const tokenPayload = secretUtils.verify<{ sub: string; deviceId: string }>(req.query.token as string);
    if (tokenPayload) {
        let user = await User.findOne({ googleUserId: tokenPayload['sub'] });
        const device = user.devices.find((device) => device._id.equals(tokenPayload.deviceId));
        if (device) {
            device.verified = true;
            user.markModified('devices');
            await user.save();

            // TODO: Create a cool html design !
            return res.send(
                `<h1>Localisation authorisé avec succès !</h1>` +
                    `<h2>Vous pouvez maintenant vous connecter par l'application.</h2>`
            );
        }
    }

    // TODO: Create a cool html design !
    res.send(`<h1>Impossible d'authoriser la localisation.</h1>`);
}
