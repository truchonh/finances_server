import { Category, HydratedCategory, LeanCategory } from '../../data/categoryCollection';
import * as _ from 'lodash';
import * as dayjs from 'dayjs';
import { ClientError } from '../../utils/errors';
import { Budget, BudgetDocument, HydratedBudget } from '../../data/budgetCollection';
import { BudgetEntryDocument } from '../../data/budgetEntryCollection';
import * as dateHelper from '../../utils/dateUtil';
import { BudgetModelBuilder } from '../../models/BudgetModelBuilder';
import { createPlannedTransactionMatcher } from '../transaction/transactionController';
import { LeanTransaction, Transaction } from '../../data/transactionCollection';
import { PlannedTransaction } from '../../data/plannedTransactionCollection';
import { splitTimeSpanByYear } from '../timeline/timelineController';
import { CategoryInfo } from '../../models/BudgetModel';
import { CategoryType } from '../../models/enums';
import { BudgetEntry as BudgetEntryDto, Category as CategoryDto } from '../../models/entities';
import { getLastActivity } from '../category/categoryController';
import NTree, { TreeNode } from '../../models/nTree';

export async function getCategories(budget: BudgetDocument): Promise<Array<HydratedCategory | LeanCategory>> {
    if (budget.isClosed) {
        return budget.categoriesSnapshot || [];
    } else {
        return Category.find().forUser(budget.userId).lean();
    }
}

export async function budgetTimeTravel(userId: string, targetYear: number) {
    const budgets = await Budget.find({ userId }).sort('year');
    // fixme: this validation assume that there is not gap year between budgets
    if (_.isEmpty(budgets) || targetYear < _.first(budgets).year || _.last(budgets).year < targetYear) {
        throw new ClientError(`Aucun budget n'existe pour l'année choisie.`);
    }

    // hide every budget (this will snapshot the budget that's currently active, regardless of if its before or
    // after the target date)
    const budgetsToHide = budgets.filter((_budget) => !_budget.isHidden);
    await Promise.all(budgetsToHide.map((_budget) => hideBudget(_budget)));

    // un-hide the budgets before the target (without restoring categories)
    const budgetToUnHide = budgets.filter((_budget) => _budget.year < targetYear && _budget.isHidden);
    await Budget.updateMany(
        { _id: { $in: budgetToUnHide.map((_budget) => _budget._id) } },
        { $set: { isHidden: false } }
    );

    // restore the target budget
    const targetBudget = budgets.find((_budget) => _budget.year === targetYear);
    await restoreBudget(targetBudget);
}

async function hideBudget(budget: BudgetDocument) {
    if (!budget.isClosed) {
        budget.categoriesSnapshot = await Category.find().forUser(budget.userId);
    }
    budget.isHidden = true;

    await budget.save();
}

async function restoreBudget(budget: BudgetDocument) {
    await Category.deleteMany({ userId: budget.userId });
    await Category.insertMany(budget.categoriesSnapshot);
    budget.isHidden = false;
    budget.categoriesSnapshot = [];
    await budget.save();
}

async function filterRelatedTransactions(budgetEntry: BudgetEntryDocument, transactions: LeanTransaction[]) {
    if (!budgetEntry.isComplex) {
        return null;
    }

    const maximumSloppinessInDays = budgetEntry.getMaximumSloppinessInDays();

    const budget: BudgetDocument = (<any>budgetEntry).parent();
    const category =
        budget.categoriesSnapshot.find((category) => category._id.equals(budgetEntry.categoryId)) ||
        (await Category.findOne({
            _id: budgetEntry.categoryId.toString(),
        })
            .forUser<LeanCategory>(budget.userId)
            .lean());
    const baseCategories = await Category.findChildBaseCategories(budget.userId, category.name);
    const baseCategoryNames = baseCategories.map((category) => category.name);

    const _transactions = transactions.filter((_t) => baseCategoryNames.includes(_t.category));
    if (_.isEmpty(_transactions)) {
        return null;
    }

    const startDate = _.minBy(
        _transactions.map((_t) => dayjs(_t.date)),
        (_d) => _d.valueOf()
    );
    const endDate = _.maxBy(
        _transactions.map((_t) => dayjs(_t.date)),
        (_d) => _d.valueOf()
    );
    const plannedTransactions = budgetEntry.getPlannedTransactions(
        startDate.subtract(maximumSloppinessInDays, 'day'),
        endDate.add(maximumSloppinessInDays, 'day')
    );

    return createPlannedTransactionMatcher(_transactions, plannedTransactions, budgetEntry);
}

async function findTransactionsForBudgetEntry(budgetEntry: BudgetEntryDocument) {
    const budget: BudgetDocument = (<any>budgetEntry).parent();

    // account for a bit of slop in the dates ..
    const startDate = dayjs().year(budget.year).startOf('year').subtract(3, 'day');
    const endDate = dayjs().year(budget.year).endOf('year').add(3, 'day');

    const category =
        budget.categoriesSnapshot.find((_cat) => _cat._id.equals(budgetEntry.categoryId)) ||
        (await Category.findOne({
            _id: budgetEntry.categoryId.toString(),
        })
            .forUser<LeanCategory>(budget.userId)
            .lean());
    if (!category) {
        return [];
    }
    const baseCategories = await Category.findChildBaseCategories(budget.userId, category.name);
    return Transaction.find({
        date: {
            $gte: startDate.toISOString(),
            $lt: endDate.toISOString(),
        },
        category: { $in: baseCategories.map((_c) => _c.name) },
    })
        .forUser(budget.userId)
        .sort('date')
        .lean();
}

type BudgetStatusesOptions = {
    userId: string;
    selectedCategoryIds: string[];
    size: number;
};
export async function getBudgetStatuses({ userId, selectedCategoryIds, size }: BudgetStatusesOptions) {
    const budgetModel = await BudgetModelBuilder.create({
        userId,
        modelYear: dayjs().year(),
        from: dayjs().subtract(3, 'month'),
        to: dayjs(),
    })
        .appendTransactionCount()
        .build();

    const findNodesWithBudget = (nodes: TreeNode<CategoryInfo>[]): TreeNode<CategoryInfo>[] => {
        const nodesWithBudget = [];
        for (let node of nodes) {
            const { budgetEntry } = node.value;
            if (budgetEntry && !budgetEntry.isComplex) {
                nodesWithBudget.push(node);
            } else {
                nodesWithBudget.push(...findNodesWithBudget(node.children));
            }
        }
        return nodesWithBudget;
    };
    const nodesWithBudget = findNodesWithBudget(budgetModel.getRootNodes());

    const budget = budgetModel.budget;
    const categories = budgetModel.categories;

    const mostActiveCategories = _.chain(nodesWithBudget)
        .sortBy((node) => -node.value.transactionCount)
        .filter((node) => !selectedCategoryIds.includes(node.value.category._id.toString()))
        .map((node) => node.value.category)
        .value();
    const selectedCategories = _.chain(selectedCategoryIds)
        .map((categoryId) => categories.find((_c) => _c._id.toString() === categoryId))
        .filter(Boolean)
        .concat(mostActiveCategories)
        .slice(0, size)
        .value();

    const budgets = [];
    for (let category of selectedCategories) {
        const budgetEntry = budget?.findEntryByCategory(category._id.toString());

        // range of date defaults to the current month, or the period if it's a simple budget
        let startDate =
            !budgetEntry || budgetEntry.isComplex
                ? dateHelper.startOfPeriod('t')
                : dateHelper.startOfPeriod(budgetEntry.period);
        let endDate =
            !budgetEntry || budgetEntry.isComplex
                ? dateHelper.endOfPeriod('t')
                : dateHelper.endOfPeriod(budgetEntry.period);

        const subCategoryNames = budgetModel
            .findNode((node) => node.value.category.name === category.name)
            .getLeaves()
            .map((value) => value.category.name);

        const medianQuery = {
            userId: userId,
            category: subCategoryNames,
            date: {
                $gte: dayjs().subtract(3, 'month').toDate(),
            },
        };
        const count = await Transaction.countDocuments(medianQuery);
        const medianAmountRes = await Transaction.find(medianQuery)
            .sort({ amount: 1 })
            .skip(Math.max(count / 2 - 1, 0))
            .limit(1);

        // if a budget entry exists, use the planned amount specified by the user. Otherwise, calculate it.
        const tempModel = await BudgetModelBuilder.create({
            userId: userId,
            modelYear: dayjs().year(),
            from: startDate,
            to: endDate,
        })
            .trimToRoots([category._id.toString()])
            .appendObservedAmount()
            .appendPlannedAmount()
            .build();
        const rootNode = _.first(tempModel.getRootNodes());
        const plannedAmount = rootNode.value?.plannedAmount || 0;
        const observedAmount = rootNode.value?.observedAmount || 0;

        budgets.push({
            categoryId: category._id.toString(),
            observedAmount: Math.abs(observedAmount),
            medianAmount: !_.isEmpty(medianAmountRes) && Math.abs(medianAmountRes[0].amount),
            plannedAmount,
            categoryName: category.name,
            period: (budgetEntry && !budgetEntry.isComplex && budgetEntry.period) || 't',
        });
    }

    return budgets;
}

export async function replacePlannedTransactions(budgetYear: number, budgetEntry: BudgetEntryDocument) {
    const transactions = await findTransactionsForBudgetEntry(budgetEntry);
    const map = (await filterRelatedTransactions(budgetEntry, transactions))?.pair() || new Map();

    const yearStart = dayjs().year(budgetYear).startOf('year');
    let yearEnd = dayjs().year(budgetYear).endOf('year');
    const isCurrentBudget = budgetYear === dayjs().year();
    if (isCurrentBudget) {
        yearEnd = yearEnd.add(3, 'month');
    }

    const plannedTransactions = budgetEntry.getPlannedTransactions(yearStart, yearEnd).map((tuple) => {
        const transactionIds = [...map.entries()]
            .filter(([_transactionId, plannedTransaction]) => {
                return plannedTransaction.date.getTime() === tuple.date.getTime();
            })
            .map(([transactionId]) => transactionId);
        return {
            amount: tuple.amount,
            date: tuple.date,
            budgetEntryId: budgetEntry._id,
            matchedTransactions: transactionIds,
        };
    });
    await PlannedTransaction.replaceAutoGeneratedItems(budgetEntry._id, plannedTransactions);
}

export async function updatePlannedTransactions(userId: string, transactions: LeanTransaction[]) {
    if (_.isEmpty(transactions)) {
        return;
    }

    const startDate = _.minBy(
        transactions.map((_t) => dayjs(_t.date)),
        (_d) => _d.valueOf()
    );
    const endDate = _.maxBy(
        transactions.map((_t) => dayjs(_t.date)),
        (_d) => _d.valueOf()
    );
    for (const { yearStart } of splitTimeSpanByYear(startDate, endDate)) {
        const currentBudget = await Budget.get(userId, yearStart.year(), true);
        if (!currentBudget) {
            continue;
        }

        for (const entry of currentBudget.entries.filter((e) => e.isComplex)) {
            const map = (await filterRelatedTransactions(entry, transactions))?.pair() || new Map();
            const plannedTransactions = await PlannedTransaction.find({
                budgetEntryId: entry._id,
                autoGenerated: true,
            });
            for (const plannedTransaction of plannedTransactions) {
                plannedTransaction.matchedTransactions = [...map.entries()]
                    .filter(([_transactionId, _plannedTransaction]) => {
                        return dayjs(_plannedTransaction.date).valueOf() === dayjs(plannedTransaction.date).valueOf();
                    })
                    .map(([transactionId]) => transactionId);
                if (!_.isEmpty(plannedTransaction.matchedTransactions)) {
                    await plannedTransaction.save();
                }
            }
        }
    }
}

/**
 * Delete sub-categories that don't match the following conditions:
 * - A transaction in the past 365 days is bound to that sub-category
 * - It was created less than a year ago
 * - A budget entry exists for that sub-category
 */
export async function clearUnUsedCategories(budget: BudgetDocument) {
    let categories = ((await getCategories(budget)) || []).filter((category) => category.type === CategoryType.base);
    const previousYearActivityMap = await getLastActivity(
        budget.userId,
        dayjs().year(budget.year).startOf('year'),
        dayjs().year(budget.year).endOf('year'),
        categories
    );
    const namesToKeep = _.keys(previousYearActivityMap).map(
        (categoryId) => categories.find((c) => c._id.equals(categoryId)).name
    );
    categories = await Category.find().ofBaseType().forUser(budget.userId);
    const currentYearActivityMap = await getLastActivity(
        budget.userId,
        dayjs().startOf('year'),
        dayjs().endOf('year'),
        categories
    );
    namesToKeep.push(
        ..._.keys(currentYearActivityMap).map((categoryId) => categories.find((c) => c._id.equals(categoryId)).name)
    );
    namesToKeep.push(
        ...budget.entries.map((entry) => categories.find((c) => c._id.equals(entry.categoryId))?.name).filter(Boolean)
    );
    await Category.deleteMany({
        name: {
            $nin: _.uniq(namesToKeep),
        },
        type: CategoryType.base,
        creationDate: {
            $lt: dayjs().subtract(1, 'year').toDate(),
        },
    });
}

export function validateBudgetEntry(budgetEntry: BudgetEntryDto, category: CategoryDto) {
    if (budgetEntry.isComplex) {
        if (_.isEmpty(budgetEntry.rules)) {
            throw new ClientError(`Vous devez configurer au moins une règle pour définir le budget.`);
        } else if (category.type !== CategoryType.base) {
            throw new ClientError(`Seule les sous-catégories peuvent avoir un budget avancé.`);
        }
    } else {
        if (!budgetEntry.period) {
            throw new ClientError(`La période doit être spécifié.`);
        } else if (!['w', 'M', 't', 'y'].includes(budgetEntry.period)) {
            throw new ClientError(`Période non-valide pour un budget simple.`);
        } else if (budgetEntry.amount === null) {
            throw new ClientError(`Vous devez spécifier le montant.`);
        }
    }
}

export async function createBudget(userId: string, year: number) {
    // Close the current budget, if there is one
    let budgets = await Budget.find({
        userId: userId,
    }).sort('-year');
    let activeBudget = budgets[0];
    if (activeBudget && !activeBudget.isClosed) {
        activeBudget.categoriesSnapshot = await Category.find().forUser(userId).lean();
        await activeBudget.save();
    }

    // Create the new one and return the appropriate data
    let budget = new Budget({
        userId: userId,
        year,
        entries: activeBudget ? activeBudget.cloneEntriesForYear(year) : [],
    });
    await budget.save();

    await clearUnUsedCategories(activeBudget);

    // cleanup and re-generate planned transactions
    await PlannedTransaction.deleteMany({
        budgetEntryId: {
            $in: activeBudget.entries.map((e) => e._id.toString()),
        },
        date: { $gte: dayjs().year(year).startOf('year') },
    });
    for (const entry of budget.entries) {
        await replacePlannedTransactions(budget.year, entry);
    }

    return budget;
}

export async function getAndValidateData(userId: string, year: number, categoryId: string) {
    const budget = await Budget.get(userId, year);
    if (!budget) {
        throw new ClientError(`Aucun budget pour l'année '${year}'`);
    }

    const category =
        budget.categoriesSnapshot.find((_cat) => _cat._id.equals(categoryId)) ||
        (await Category.findOne({ _id: categoryId }).forUser(userId).lean());
    if (!category) {
        throw new ClientError(`The category does not exist.`, 404);
    }

    return { budget, category };
}

export function prepareCategoryTree(categoryNode: TreeNode<LeanCategory>): CategoryDto {
    const category: CategoryDto = {
        ...categoryNode.value,
        children: categoryNode.children.map((_childNode) => prepareCategoryTree(_childNode)),
    };
    return Category.toDto(category);
}

export async function prepareBudgetForClient(userId: string, budget: HydratedBudget) {
    let categories = (budget && (await getCategories(budget))) || (await Category.find().forUser(userId).lean());
    let categoryTree = new NTree(
        categories,
        (c) => c.name,
        (c) => c.parent
    );
    let categoryTrees = _.chain(categoryTree.getRootNodes())
        .map((_rootNode) => prepareCategoryTree(_rootNode))
        .sortBy((_category) => _.deburr(_category.name))
        .value();
    return Budget.toDto(budget, categoryTrees);
}
