/**
 * Created by hugo on 2020-03-04
 */
import { AuthenticatedRequest, makeAuthenticatedRouter, makeRequestHandler, toInt } from '../../utils/queryUtil';
import { Budget, LeanBudget } from '../../data/budgetCollection';
import * as _ from 'lodash';
import * as dayjs from 'dayjs';
import { BudgetModelBuilder } from '../../models/BudgetModelBuilder';
import * as dateHelper from '../../utils/dateUtil';
import { SystemCategory } from '../../models/enums';
import { CategoryInfo } from '../../models/BudgetModel';
import { TreeNode } from '../../models/nTree';
import { ClientError } from '../../utils/errors';
import {
    createBudget,
    getBudgetStatuses,
    replacePlannedTransactions,
    budgetTimeTravel,
    validateBudgetEntry,
    getAndValidateData,
    prepareBudgetForClient,
} from './budgetController';
import { Category } from '../../data/categoryCollection';
import { BudgetEntry as BudgetEntryDto } from '../../models/entities';
import { BudgetEntryDocument } from '../../data/budgetEntryCollection';

// prettier-ignore
export default function getBudgetRouter() {
    const router = makeAuthenticatedRouter();

    router.route('/:year/category_summary')
        .get(makeRequestHandler(categorySummary));
    router.route('/status')
        .get(makeRequestHandler(getStatus));
    router
        .route('/:year/category/:category_id')
        .get(makeRequestHandler(getBudgetEntry))
        .put(makeRequestHandler(putCategoryBudget))
        .delete(makeRequestHandler(deleteCategoryBudget));
    router.route('/:year')
        .get(makeRequestHandler(getBudget))
        .post(makeRequestHandler(create));
    router.route('/time_travel/:target_year')
        .put(makeRequestHandler(timeTravel));
    router.route('/')
        .get(makeRequestHandler(listBudgets));

    return router;
}

async function listBudgets(req: AuthenticatedRequest) {
    const budgets = await Budget.find()
        .forUser<LeanBudget[]>(req.userId)
        .sort('year')
        .select('_id categoriesSnapshot year isHidden')
        .lean();
    return {
        items: budgets.map((_budget) => ({
            ..._budget,
            isClosed: !_.isEmpty(_budget.categoriesSnapshot),
            categoriesSnapshot: undefined,
        })),
        isTimeTravel: !_.isEmpty(budgets) && _.last(budgets).isHidden,
    };
}

type CategorySummaryEntry = {
    name: string;
    hue: string;
    observed: number;
    planned: number | null;
    plannedAdjusted: number | null;
};
type CategorySummary = {
    hasBudget: boolean;
    budgetYear: number;
    isBudgetClosed: boolean;
    revenue?: Array<CategorySummaryEntry>;
    spending?: Array<CategorySummaryEntry>;
};
async function categorySummary(req: AuthenticatedRequest) {
    const params = req.params;
    const year = toInt(params.year);
    const query = req.query;
    const from = (query.from && dayjs(query.from?.toString())) || dateHelper.startOfYear(year);
    const to = (query.to && dayjs(query.to?.toString())) || dateHelper.endOfYear(year);
    const categoryIds = query.categories?.toString()?.split(',');

    const budgetModel = await BudgetModelBuilder.create(
        {
            userId: req.userId,
            modelYear: year,
            from,
            to,
        },
        { ignoreUncategorized: !_.isEmpty(categoryIds) }
    )
        .trimToRoots(categoryIds)
        .appendObservedAmount()
        .appendPlannedAdjustedAmount()
        .appendTransactionCount()
        .build();

    const budget = budgetModel.budget;
    let nonTransferRootNodes = budgetModel
        .getRootNodes()
        .filter(({ value: { category } }) => category.systemType !== SystemCategory.transfer);
    const formatBudgetSummaryReducer = (
        response: CategorySummary,
        categoryNodes: TreeNode<CategoryInfo>[],
        index: number
    ) => ({
        ...response,
        [index === 0 ? 'revenue' : 'spending']: _.chain(categoryNodes)
            .map(({ value: { category, plannedAmount, observedAmount, plannedAdjustedAmount } }) => ({
                name: category.name,
                hue: category.hue || '',
                observed: Math.abs(observedAmount),
                planned: plannedAmount || null,
                plannedAdjusted: plannedAdjustedAmount || null,
            }))
            .filter((_entry) => !!_entry.planned || !!_entry.observed)
            .sortBy((entry) => _.deburr(entry.name))
            .value(),
    });
    return _.partition(
        nonTransferRootNodes,
        ({ value: { category } }) => category.systemType === SystemCategory.income
    ).reduce(formatBudgetSummaryReducer, {
        hasBudget: !!budget,
        budgetYear: budget?.year,
        isBudgetClosed: budget?.isClosed || false,
    });
}

async function getBudget(req: AuthenticatedRequest) {
    let year = toInt(req.params.year);
    let budget = await Budget.get(req.userId, year, true);

    if (!budget) {
        throw new ClientError(`Pas de budget pour l'année ${year}.`);
    }

    return await prepareBudgetForClient(req.userId, budget);
}

/**
 * Create the budget of the year, if it does not exist and if the current budget can be closed.
 * A budget can be closed starting at the middle of the year.
 */

async function create(req: AuthenticatedRequest) {
    let year = toInt(req.params.year);
    let now = dayjs();
    if (year < now.year()) {
        throw new ClientError(`Impossible de créer un budget pour une année passé.`);
    }
    let existingBudget = await Budget.findOne({ userId: req.userId, year }).select('_id').lean();
    if (existingBudget) {
        throw new ClientError(`Un budget existe déjà pour l'année ${year}.`);
    }
    if (year > now.year()) {
        throw new ClientError(`Le budget actif ne peut pas être fermé avant la fin de l'année.`);
    }

    const budget = await createBudget(req.userId, year);
    return await prepareBudgetForClient(req.userId, budget);
}

/**
 * Get a single budget entry and related category
 */
async function getBudgetEntry(req: AuthenticatedRequest) {
    const { category, budget } = await getAndValidateData(req.userId, toInt(req.params.year), req.params.category_id);
    return {
        category: Category.toDto(category),
        budgetEntry: budget.findEntryByCategory(req.params.category_id),
    };
}

/**
 * Create or update a budget entry for that category.
 */
async function putCategoryBudget(req: AuthenticatedRequest) {
    const { budget, category } = await getAndValidateData(req.userId, toInt(req.params.year), req.params.category_id);
    const categoryId = req.params.category_id;

    const budgetEntry: BudgetEntryDto = _.pick(req.body, [
        'categoryId',
        'isComplex',
        'amount',
        'period',
        'preset',
        'rules',
    ]);
    validateBudgetEntry(budgetEntry, category);

    let existingBudgetEntry = budget.findEntryByCategory(categoryId);
    if (existingBudgetEntry) {
        // Updating an existing entry
        existingBudgetEntry.isComplex = budgetEntry.isComplex;
        existingBudgetEntry.amount = budgetEntry.amount;
        existingBudgetEntry.period = budgetEntry.period;
        existingBudgetEntry.preset = budgetEntry.preset;
        existingBudgetEntry.rules = budgetEntry.rules;
    } else {
        budget.entries.push(budgetEntry as BudgetEntryDocument);
    }

    await budget.save();

    existingBudgetEntry = budget.findEntryByCategory(categoryId);
    await replacePlannedTransactions(budget.year, existingBudgetEntry);
}

/**
 * Delete a budget entry for that category.
 */
async function deleteCategoryBudget(req: AuthenticatedRequest) {
    let year = toInt(req.params.year);
    let categoryId = req.params.category_id;

    let budget = await Budget.get(req.userId, year);
    if (!budget) {
        throw new ClientError(`No budget for year '${year}'`);
    }

    let budgetEntry = budget.findEntryByCategory(categoryId);
    if (!budgetEntry) {
        throw new ClientError(`No budget entry for category '${categoryId}'`);
    }

    await budgetEntry.remove();
    await budget.save();
}

/**
 * Get the status of the most active budgets. Specific categoryIds can be specified, as well as the number of entries
 * that should be returned.
 */
async function getStatus(req: AuthenticatedRequest) {
    /**
     * @property categories
     * @property size
     */
    const query = req.query;
    const selectedCategoryIds = (query.categories && query.categories?.toString().split(',')) || [];
    const size = toInt(query.size?.toString()) || 3;

    return {
        budgets: await getBudgetStatuses({
            userId: req.userId,
            selectedCategoryIds,
            size,
        }),
    };
}

async function timeTravel(req: AuthenticatedRequest) {
    const params = req.params;
    const targetYear = toInt(params.target_year);

    await budgetTimeTravel(req.userId, targetYear);
}
