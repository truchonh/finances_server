/**
 * Created by hugo on 2018-05-13.
 */
import * as _ from 'lodash';
import * as FileDao from '../../data/fileCollection';
import { parseStatementFileType, StatementFileType } from '../../models/enums';
import { EssentialTransactionData } from '../../models/entities';
import { extractAndValidateStatementData } from '../../utils/multipartUtil';
import { iterateDiffs } from '../../utils/transactionUtils';
import { defaultContext } from '../../utils/context';
import { Statement } from '../../data/statementCollection';
import { LeanTransaction, Transaction } from '../../data/transactionCollection';
import { createStatementWithTransactions, removeEmptyStatements } from './statementController';
import { removeTransactionsById, findExistingTransactions } from '../transaction/transactionController';
import { findAndCreateNewCategories } from '../category/categoryController';
import { updateAccountsFromStatement } from '../account/accountController';
import {
    AuthenticatedRequest,
    makeAuthenticatedRouter,
    makeRequestHandler,
    toBoolean,
    toInt,
    toObject,
} from '../../utils/queryUtil';
import { createCacheExpirationEvents } from '../../caching/clientCache';
import { deleteTransactionsByStatement, indexTransactions } from '../../data/elasticsearch/elasticsearch';

export default function getStatementRouter() {
    const router = makeAuthenticatedRouter();

    router.route('/approved/:is_approved').get(makeRequestHandler(getByApprovedState));
    router.route('/count').get(makeRequestHandler(countApproved));
    router.route('/history').get(makeRequestHandler(getRecent));
    router
        .route('/:statement_id')
        .all(defaultContext)
        .get(makeRequestHandler(getStatement))
        .delete(makeRequestHandler(deleteStatement));
    router.route('/:statement_id/approve').all(defaultContext).put(makeRequestHandler(approveStatement));
    router
        .route('/')
        .get(makeRequestHandler(getMany))
        .post(makeRequestHandler(create))
        .put(makeRequestHandler(replace));

    return router;
}

// GET  /api/statement/approved/:is_approved
async function getByApprovedState(req: AuthenticatedRequest) {
    let params = req.params;

    let statements = await Statement.find({
        isApproved: toBoolean(params.is_approved) || false,
    })
        .forUser(req.userId)
        .lean();
    return { statements };
}

// GET  /api/statement?size={number}&skip={number}&sort={object}&filter={object}
async function getMany(req: AuthenticatedRequest) {
    const query = req.query;

    const size = toInt(query.size?.toString() || '50');
    const skip = toInt(query.skip?.toString() || '0');
    const filter = toObject(query.filter?.toString()) || {};
    const sort = toObject(query.sort?.toString()) || {};

    const statements = await Statement.searchForUser(req.userId, size, skip, filter, sort);
    return { items: statements.map((_s) => _s.toDto()) };

    // TODO: - Add the optional field function call here
    //       - Implement the transaction_count and last_transaction_date fields
}

async function getRecent(req: AuthenticatedRequest) {
    let size = toInt(req.query.size?.toString()) || 40;
    let skip = toInt(req.query.skip?.toString()) || 0;

    let statementInfo = await Statement.aggregateTransactionCount(req.userId, size, skip);

    return {
        importHistory: statementInfo.map((s) => {
            // @ts-ignore
            s.fileType = parseStatementFileType(parseInt(s.fileType));
            return {
                id: s._id?.toString() || s.id,
                date: s.date,
                creationDate: s.creationDate,
                fileType: s.fileType,
                isApproved: s.isApproved,
                fileId: s.fileId,
                statementData: s,
                lastTransactionDate: s.lastTransactionDate,
                transactionCount: s.transactionCount,
            };
        }),
    };
}

// GET  /api/statement/count
async function countApproved(req: AuthenticatedRequest) {
    let count = await Statement.countDocuments({
        userId: req.userId,
        isApproved: false,
    });
    return { count };
}

// GET  /api/statement/:statement_id
async function getStatement(req: AuthenticatedRequest) {
    return { statement: req.context.statement_id };
}

// DELETE   /api/statement/:statement_id
async function deleteStatement(req: AuthenticatedRequest) {
    const statement = req.context.statement_id;

    await Transaction.deleteMany({ statementId: req.params.statement_id });
    await deleteTransactionsByStatement(req.params.statement_id);
    if (statement.fileType !== StatementFileType.webScraper) {
        await FileDao.remove(statement.fileId);
    }

    await statement.remove();
}

// PUT  /api/statement/:statement_id/approve
async function approveStatement(req: AuthenticatedRequest) {
    const statementId = req.params.statement_id;
    const statement = req.context.statement_id;

    let transactions = await Transaction.find({ statementId: statementId }).forUser(req.userId);

    await deleteTransactionsByStatement(statementId);
    await indexTransactions(transactions);

    statement.isApproved = true;
    await statement.save();
}

// POST /api/statement
async function create(req: AuthenticatedRequest) {
    let insertedStatement;
    let cacheExpirationEvents = ['STATEMENT_UNAPPROVED'];

    if (req.headers['content-type'].includes('multipart/form-data')) {
        // assume we are receiving a file and transactions data
        const { statement, file } = await extractAndValidateStatementData(req);

        statement.transactions.sort((a, b) => {
            return new Date(a.date).getTime() - new Date(b.date).getTime();
        });
        insertedStatement = await createStatementWithTransactions({
            userId: req.userId,
            transactions: statement.transactions,
            date: statement.transactions[0].date,
            fileType: statement.fileType,
            fileId: file._id,
        });

        await updateAccountsFromStatement(req.userId, statement.transactions, file._id, statement.fileType);

        await createCacheExpirationEvents(req.user, statement.transactions);
    } else {
        insertedStatement = new Statement({
            userId: req.userId,
            date: req.body.date,
            fileType: req.body.fileType,
        });
        await insertedStatement.save();
    }

    await req.user.addCacheExpirationEvents(cacheExpirationEvents);

    return {
        statement: insertedStatement.toDto(),
    };
}

// PUT  /api/statement
async function replace(req: AuthenticatedRequest) {
    const { statement, file } = await extractAndValidateStatementData(req);
    const existingTransactions = await findExistingTransactions(statement.transactions, req.userId);

    const unchangedTransactionPairs: [EssentialTransactionData, LeanTransaction][] = [];
    const newTransactions: EssentialTransactionData[] = [];
    let removedTransactions: LeanTransaction[] = [];
    iterateDiffs(statement.transactions, existingTransactions, (added, removed) => {
        if (added && removed) {
            unchangedTransactionPairs.push([added, removed]);
        } else if (added) {
            newTransactions.push(added);
        } else if (removed) {
            removedTransactions.push(removed);
        }
    });

    await findAndCreateNewCategories(req.userId, newTransactions);

    const statementTransactions = [...unchangedTransactionPairs.map((pair) => pair[1]), ...newTransactions];
    const transactionDates = _.chain(statementTransactions)
        .map((_t) => _t.date)
        .sortBy((_date) => _date.getTime())
        .value();
    let newStatement = await createStatementWithTransactions({
        userId: req.userId,
        transactions: newTransactions,
        date: _.first(transactionDates),
        fileType: statement.fileType,
        fileId: file._id,
    });
    for (let pair of unchangedTransactionPairs) {
        await Transaction.updateOne(
            { _id: pair[1]._id },
            {
                $set: {
                    statementId: newStatement._id,
                    description: pair[0].description,
                },
            }
        );
    }

    // Sometimes, transactions of a particular day will be split between two statements, if that day is the
    // first day of the statement (and the last day of the previous statement).
    // Let two statements, A and B, where statement A ends the same day statement B starts.
    // If B is uploaded through this route, existing transactions dated for the 1st day of B will be deleted.
    // ---> This is why we filter out removed transactions this next line :
    removedTransactions = removedTransactions.filter(
        (_t) =>
            _t.date.getTime() !== _.first(transactionDates).getTime() &&
            _t.date.getTime() !== _.last(transactionDates).getTime()
    );
    await removeTransactionsById(removedTransactions.map((_t) => _t._id.toString()));

    const overlappingStatementIds = _.uniq(
        [...unchangedTransactionPairs.map((pair) => pair[1]), ...removedTransactions].map((_t) =>
            _t.statementId?.toString()
        )
    );
    await removeEmptyStatements(overlappingStatementIds);

    await updateAccountsFromStatement(req.userId, statement.transactions, file._id, statement.fileType);

    await createCacheExpirationEvents(req.user, statement.transactions);
    await req.user.addCacheExpirationEvents(['STATEMENT_UNAPPROVED']);

    return {
        statement: newStatement,
    };
}
