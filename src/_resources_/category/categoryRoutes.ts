/**
 * Created by Hugo on 2018-07-08
 */
import { FilterQuery } from 'mongoose';
import * as dayjs from 'dayjs';
import * as _ from 'lodash';
import { AuthenticatedRequest, makeAuthenticatedRouter, makeRequestHandler } from '../../utils/queryUtil';
import { Category, HydratedCategory, ICategory } from '../../data/categoryCollection';
import { defaultContext } from '../../utils/context';
import { ClientError } from '../../utils/errors';
import { deleteCategoryAndReferences, getLastActivity, updateCategoryAndReferences } from './categoryController';
import { CategoryType } from '../../models/enums';
import { LeanStatement, Statement } from '../../data/statementCollection';
import { getRelevantCategories } from '../../data/elasticsearch/elasticsearch';

export default function getCategoryRouter() {
    const router = makeAuthenticatedRouter();

    router.route('/').get(makeRequestHandler(getCategories)).put(makeRequestHandler(updateCategory));
    router.route('/count').get(makeRequestHandler(countUncategorized));
    router.route('/sub_categories/:category_name').get(makeRequestHandler(getSubCategories));
    router.route('/last_activity').get(makeRequestHandler(lastActivity));
    router.route('/suggest_match/:transaction_id').all(defaultContext).get(makeRequestHandler(suggestMatch));
    router.route('/:category_type').get(makeRequestHandler(getByType));
    router.route('/:category_id').all(defaultContext).delete(makeRequestHandler(delete_));

    return router;
}

async function getCategories(req: AuthenticatedRequest) {
    let query = req.query;

    let dbQuery: FilterQuery<HydratedCategory> = {};

    if (query.name) {
        dbQuery['name'] = decodeURIComponent(query.name?.toString());
    }
    let categories = await Category.find(dbQuery).forUser(req.userId).lean();

    return {
        categories: categories.map((c) => Category.toDto(c)),
    };
}

async function updateCategory(req: AuthenticatedRequest) {
    let body = req.body;
    let categoryDto = Category.toDto(body);

    if (!categoryDto.name) {
        throw new ClientError(`La catégorie doit avoir un nom.`);
    } else if (categoryDto.name === categoryDto.parent) {
        throw new ClientError(`Référence de parent circulaire.`);
    }

    const updatedCategory = await updateCategoryAndReferences(req.userId, categoryDto);
    return updatedCategory.toDto();
}

async function countUncategorized(req: AuthenticatedRequest) {
    let count = await Category.countDocuments({
        userId: req.userId,
        type: CategoryType.base,
        parent: null,
    });
    return {
        count: count,
    };
}

async function getSubCategories(req: AuthenticatedRequest) {
    const category = req.params.category_name?.toString();
    const subCategories = await Category.findChildBaseCategories(req.userId, category);
    return {
        items: subCategories.map((c) => Category.toDto(c as any)),
    };
}

async function getByType(req: AuthenticatedRequest) {
    let params = req.params;
    let query = req.query;

    if (params.category_type) {
        let parentCategories = await Category.find({
            type: params.category_type,
        })
            .forUser(req.userId)
            .lean();
        let categories = parentCategories.map((c) => Category.toDto(c));

        if (query.include_children) {
            for (let category of categories) {
                let children = await Category.find({
                    parent: category.name,
                })
                    .forUser(req.userId)
                    .select('name')
                    .lean();
                category.children = children.map((c) => c.name);
            }
        }

        return { categories };
    } else {
        throw new ClientError(`Category cannot be queried for field(s): ${Object.keys(query)}`, 404);
    }
}

async function lastActivity(req: AuthenticatedRequest) {
    const query = req.query;
    const from = (query.from && dayjs(query.from.toString())) || null;
    const to = (query.to && dayjs(query.to.toString())) || null;
    return {
        activityMap: await getLastActivity(req.userId, from, to),
    };
}

async function delete_(req: AuthenticatedRequest) {
    await deleteCategoryAndReferences(req.userId, req.context.category_id);
}

async function suggestMatch(req: AuthenticatedRequest) {
    const transaction = req.context.transaction_id;

    // TODO: Perfect matches (from Category.matchedDescriptions) are kind of ignoed here.
    //       If there is one, it should be at the top of the list.

    const statement = await Statement.findOne({ _id: transaction.statementId })
        .forUser<LeanStatement>(req.userId)
        .lean();
    const suggestions = await getRelevantCategories(
        req.userId,
        _.isEmpty(transaction.descriptionInfo) ? transaction.description : transaction.descriptionInfo,
        statement?.fileType
    );
    const meanScore = _.meanBy(suggestions, 'score');
    const medianScore = _.nth(suggestions, suggestions.length / 2)?.score || 0;

    return {
        categories: _.chain(suggestions)
            .filter((hit) => hit.score >= Math.max(meanScore, medianScore))
            .map((hit) => hit.value)
            .slice(0, 9)
            .value(),
    };
}
