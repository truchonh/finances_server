import { Account } from '../../data/accountCollection';
import * as _ from 'lodash';
import * as dayjs from 'dayjs';
import { isLoanAccount, parseAccountType, StatementFileType } from '../../models/enums';
import * as FileDao from '../../data/fileCollection';
import { ClientError } from '../../utils/errors';
import { EssentialTransactionData } from '../../models/entities';
import { findUniqAccounts } from '../../utils/transactionUtils';

const transactionProcessor = require('../../import/csv_processor/transactionProcessor');
const searchablePdfProcessor = require('../../import/pdf_processor/searchablePdfProcessor');

export async function updateAccountsFromStatement<T extends EssentialTransactionData>(
    userId: string,
    transactions: T[],
    fileId: string,
    fileType: StatementFileType
) {
    const accountIds = findUniqAccounts(transactions);
    const accounts = await Account.find({
        accountName: { $in: accountIds },
    })
        .forUser(userId)
        .lean();

    let statementAccounts = null;

    for (let accountId of accountIds) {
        let account = accounts.find((_a) => _a.accountName === accountId);
        let mostRecentTransactionDate = _.maxBy(
            transactions.filter((_t) => _t.account.includes(accountId)).map((_t) => dayjs(_t.date)),
            (_d) => _d.valueOf()
        );

        if (!account || dayjs(account.updatedAt).isBefore(mostRecentTransactionDate)) {
            // account either doesn't exist or a new transaction has newer balance info
            if (!statementAccounts) {
                statementAccounts = await extractAccountBalances(fileId, fileType);
            }
            const statementAccount = statementAccounts.find((_a) => _a.accountName === accountId);
            const accountType = parseAccountType(accountId);
            await Account.createOrUpdate(
                new Account({
                    _id: (account && account._id) || null,
                    accountName: accountId,
                    amount: isLoanAccount(accountType) ? statementAccount.balance * -1 : statementAccount.balance,
                    updatedAt: statementAccount.date,
                    accountType,
                }),
                userId
            );
        }
    }
}

async function extractAccountBalances(fileId: string, fileType: StatementFileType) {
    const stream = await FileDao.download(fileId);
    let accounts: Array<{ accountName: string; date: Date; balance: number }> = [];

    let _file = await FileDao.findOne({ _id: fileId });
    let fileExtension = _file.filename.split('.').pop();
    switch (fileExtension.toLowerCase()) {
        case 'csv':
            accounts = await transactionProcessor.getBalance(fileType, stream);
            break;
        case 'pdf':
            accounts = await searchablePdfProcessor.getBalance(fileType, stream);
            break;
        default:
            throw new ClientError(`Type de fichier non supporté (${fileExtension}).`);
    }

    return accounts;
}
