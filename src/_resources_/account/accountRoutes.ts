/**
 * Created by hugo on 2018-10-15
 */
import * as _ from 'lodash';
import { AuthenticatedRequest, makeAuthenticatedRouter, makeRequestHandler, toBoolean } from '../../utils/queryUtil';
import { Account } from '../../data/accountCollection';
import { Category } from '../../data/categoryCollection';
import { Transaction } from '../../data/transactionCollection';
import NTree from '../../models/nTree';
import { defaultContext } from '../../utils/context';
import { createCategoryGuesstimator } from '../../data/elasticsearch/elasticsearch';

export default function getAccountRouter() {
    const router = makeAuthenticatedRouter();

    router
        .route('/:account_id/pending_transactions')
        .all(defaultContext)
        .get(makeRequestHandler(getPendingTransactions));
    router
        .route('/:account_id')
        .all(defaultContext)
        .patch(makeRequestHandler(patch))
        .delete(makeRequestHandler(delete_));
    router.route('/').get(makeRequestHandler(getAll)).put(makeRequestHandler(createOrUpdate));

    return router;
}

async function getAll(req: AuthenticatedRequest) {
    const activeOnly = toBoolean(req.query.activeOnly?.toString());
    let accounts;
    if (activeOnly) {
        accounts = await Account.getActiveAccounts(req.userId);
    } else {
        accounts = await Account.find().forUser(req.userId);
    }
    return {
        items: _.chain(accounts)
            .sortBy((account) => -account.recordCount)
            .map((account) => account.toDto())
            .value(),
    };
}

async function createOrUpdate(req: AuthenticatedRequest) {
    const body = req.body;

    const account = await Account.createOrUpdate({ ...body, updatedAt: null }, req.userId);
    await req.user.addCacheExpirationEvents(['ACCOUNTS_UPDATED']);
    return { account: Account.toDto(account) };
}

async function patch(req: AuthenticatedRequest) {
    const allowedFields = ['displayName', 'isInvestmentAccount', 'disabled'];
    const filteredBody = _.pick(req.body, allowedFields);
    const account = req.context.account_id;

    if (filteredBody.displayName) {
        account.displayName = filteredBody.displayName;
    }
    if (filteredBody.isInvestmentAccount) {
        account.isInvestmentAccount = filteredBody.isInvestmentAccount;
    }
    if (filteredBody.disabled) {
        account.disabled = filteredBody.disabled;
    }

    await account.save();
    return account.toDto();
}

async function delete_(req: AuthenticatedRequest) {
    const account = req.context.account_id;
    await account.remove();
}

async function getPendingTransactions(req: AuthenticatedRequest) {
    const account = req.context.account_id;

    const pendingRecords = account.pendingRecords.map((record) => {
        if (record.description?.length === 41) {
            return {
                ...record,
                descriptionInfo: {
                    main: record.description.substring(0, 25),
                    location: record.description.substring(25),
                },
            };
        } else {
            return record;
        }
    });

    let categoryGuesser = await createCategoryGuesstimator(req.userId, pendingRecords);

    let categories = await Category.find().forUser(req.userId).lean();
    let categoryTree = new NTree(
        categories,
        (_cat) => _cat.name,
        (_cat) => _cat.parent
    );
    const hierarchyMap = categoryTree.getCategoryHierarchy((_category) => ({
        _id: _category._id,
        name: _category.name,
        hue: _category.hue,
        systemType: _category.systemType,
        type: _category.type,
    }));

    // convert records to transactions
    return {
        items: pendingRecords.map((_record) => {
            let categoryMatch = categoryGuesser.find(_record.descriptionInfo || _record.description);

            let categoryName: string = null;
            if (categoryMatch) {
                let category = categories.find((_cat) => _cat.name.toLowerCase() === categoryMatch.category);
                if (category) {
                    categoryName = category.name;
                }
            }

            return Transaction.toDto({
                date: _record.date,
                description: _record.description,
                descriptionInfo: _record.descriptionInfo,
                amount: _record.amount || _record.deposit || (_record.withdrawal ? _record.withdrawal * -1 : 0),
                account: _record.account,
                currency: _record.currency,
                category: categoryName,
                categoryHierarchy: hierarchyMap.get(categoryName),
            });
        }),
    };
}
