import { Request } from 'express';
import * as _ from 'lodash';
import { hashApiKey, generateClearTextApiKey } from '../../utils/secretUtils';
import { AuthenticatedRequest, makeAuthenticatedRouter, makeRequestHandler } from '../../utils/queryUtil';
import { IApiAccess } from '../../data/userCollection';
import { toMs } from 'ms-typescript';

const ipware = require('ipware');
const getIp = ipware().get_ip as (request: Request) => { clientIp: string; clientIpRoutable: boolean };

export default function getUserRouter() {
    const router = makeAuthenticatedRouter();

    router.route('/').get(makeRequestHandler(get));
    router.route('/deauthorize').put(makeRequestHandler(deauthorize));
    router.route('/api_access/:api_key_id').delete(makeRequestHandler(disableApiKey));
    router.route('/api_access').post(makeRequestHandler(generateApiKey));

    return router;
}

async function get(req: AuthenticatedRequest) {
    const ip = getIp(req).clientIp;

    return {
        ..._.omit(req.user.toJSON(), ['webScraper', 'devices', 'locations']),

        devices: req.user.devices
            .filter((_device) => _device.timestamp >= Date.now() - toMs('30d') && _device.deviceToken)
            .map((_device) => ({
                platform: _device.platform,
                manufacturer: _device.manufacturer,
                model: _device.model,
                version: _device.version,
                lastLocation: _device.lastLocation,
                timestamp: _device.timestamp,
                isCurrent: _device._id?.toString() === req.deviceId,
            })),
        locations: req.user.locations
            .filter((_location) => _location.timestamp >= Date.now() - toMs('30d'))
            .map((_location) => ({
                ip: _location.ip,
                locationLabel:
                    [_location.city, _location.region, _location.country].filter((_token) => _token).join(', ') ||
                    'Inconnue',
                timestamp: _location.timestamp,
                isCurrent: ip === _location.ip,
            })),
        apiAccess: req.user.apiAccess?.filter((access) => !access.disabled),
    };
}

async function deauthorize(req: AuthenticatedRequest) {
    // TODO: Need to rethink what this action does.
    //       We dont want to delete deviceTokens because its a pain to authorize them (email confirmation and so on)
    // const user = req.user;
    // const ip = getIp(req).clientIp;
    // const currentDevice = user.devices.find(_device => _device.ip === ip);
    // const currentLocation = user.locations.find(_location => _location.ip === ip);
    //
    // user.devices = user.devices.filter(_device => _device === currentDevice);
    // user.locations = user.locations.filter(_location => _location === currentLocation);
    //
    // user.markModified('devices');
    // user.markModified('locations');
    // await user.save();
}

async function generateApiKey(req: AuthenticatedRequest) {
    const clearTextApiKey = generateClearTextApiKey();
    const apiAccess: IApiAccess = {
        apiKey: hashApiKey(clearTextApiKey),
    };

    if (_.isEmpty(req.user.apiAccess)) {
        req.user.apiAccess = [apiAccess];
    } else {
        req.user.apiAccess.push(apiAccess);
    }
    req.user.markModified('apiAccess');

    await req.user.save();

    return {
        apiKey: clearTextApiKey,
        items: req.user.apiAccess?.filter((access) => !access.disabled),
    };
}

async function disableApiKey(req: AuthenticatedRequest) {
    const apiKeyId = req.params.api_key_id;
    const apiKey = req.user?.apiAccess?.find((access) => access._id.equals(apiKeyId));
    if (apiKey) {
        apiKey.disabled = true;
    }
    req.user.markModified('apiAccess');

    await req.user.save();
}
