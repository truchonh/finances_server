/**
 * Created by hugo on 2020-03-04
 */
import * as dayjs from 'dayjs';
import { extend, isEmpty, min, findLast } from 'lodash';
import { Schema, Model, model, Document, FilterQuery, LeanDocument, HydratedDocument } from 'mongoose';
import { forUser, IGenericSchema, GenericQueryHelpers, GenericSchema } from './genericModel';
import { addOnePeriod } from '../utils/dateUtil';
import { budgetEntrySchema, BudgetEntryDocument } from './budgetEntryCollection';
import { HydratedCategory } from './categoryCollection';
import { Budget as BudgetDto, Category as CategoryDto } from '../models/entities';

export interface IBudget extends IGenericSchema {
    year: number;
    entries: Array<BudgetEntryDocument>;
    categoriesSnapshot: Array<HydratedCategory>;
    plannedBudgetSumHistory: Array<{ month: number; value: number }>;
    isHidden: boolean;
}

interface IBudgetMethods {
    findEntryByCategory(categoryId: string): BudgetEntryDocument;
    /**
     * Copy the budget entries, filtering the ones ending before the `newYear` and changing the start dates for the
     * other ones.
     */
    cloneEntriesForYear(newYear: number): Array<BudgetEntryDocument>;
}

export interface BudgetDocument extends IBudget, Document, IBudgetMethods {
    isClosed: boolean;
}
export type LeanBudget = LeanDocument<BudgetDocument>;
export type HydratedBudget = HydratedDocument<BudgetDocument>;

interface BudgetModel extends Model<BudgetDocument, GenericQueryHelpers<BudgetModel>, IBudgetMethods> {
    get(userId: string, year: number, ignoreHidden?: boolean): Promise<HydratedBudget>;
    getClosest(userId: string, year: number, ignoreHidden?: boolean): Promise<HydratedBudget>;
    toDto(budget: LeanBudget, rootCategories: CategoryDto[]): BudgetDto;
    getActive(userId: string): Promise<HydratedBudget>;
}

/**
 * @class Budget
 */
let budgetSchema = new Schema<BudgetDocument, BudgetModel, IBudgetMethods, GenericQueryHelpers<BudgetDocument>>(
    extend(
        {
            year: { type: Number, required: true },
            /**
             * *** WHEN THIS VALUE IS SET, THE BUDGET BECOMES READ-ONLY ***
             * Only needed after the budget is closed (next one is created), to display the yearly stats as the categories
             * were when the budget was first created.
             * For example, if a category is renamed and does not reference the same shildren categories anymore, we still
             * want to display the budget stats as the category was before, otherwise the data loses context and doesn't
             * make sence anymore.
             */
            categoriesSnapshot: [Schema.Types.Mixed],
            entries: [budgetEntrySchema],
            /**
             * History of the planned yearly amount for cimple budget entries.
             */
            plannedBudgetSumHistory: [{ month: Number, value: Number }],
            isHidden: Boolean,
        },
        GenericSchema
    )
);

budgetSchema.query.forUser = forUser<BudgetDocument>;

budgetSchema.virtual('isClosed').get(function isClosed(this: BudgetDocument) {
    return !isEmpty(this.categoriesSnapshot);
});

budgetSchema.method('findEntryByCategory', function findEntryByCategory(this: BudgetDocument, categoryId: string) {
    return this.entries.find((entry) => entry.categoryId.equals(categoryId));
});

budgetSchema.method('cloneEntriesForYear', function cloneEntriesForYear(this: BudgetDocument, newYear: number) {
    const oldEntries = this.entries.map((entry) => entry.toObject());
    const updatedEntries = oldEntries.filter((entry) => !entry.isComplex);

    for (let entry of oldEntries.filter((entry) => entry.isComplex)) {
        delete entry._id;
        let updatedRules = entry.rules
            // filter rules where the end date is before the start of newYear, or rules for single
            // transactions (no period and no end date) where the start date is before the start of newYear.
            .filter((rule: any) => {
                const isEndingBeforeNewYear = rule.endDate && rule.endDate.getFullYear() < newYear;
                const isSingleTransactionRule = !rule.endDate && !rule.period;
                return !(isEndingBeforeNewYear || (isSingleTransactionRule && rule.startDate.getFullYear() < newYear));
            })
            .map((rule: any) => {
                if (rule.startDate.getFullYear() < newYear) {
                    // we have to adjust the startDate to be within the new year
                    const target = dayjs().year(newYear).startOf('year');
                    let start = dayjs(rule.startDate);

                    while (start.isBefore(target)) {
                        start = addOnePeriod(start, rule.period);
                    }

                    rule.startDate = start.toDate();
                }
                return rule;
            });
        if (isEmpty(updatedRules)) {
            // If no entries are left, force a budget of 0$.
            // This is needed because if the budget is removed, the transactions will be considered for the sumPerDay
            // calculation of planned adjusted amounts, which is not what we want.
            // TODO: This is not great, we would rather have a way to remove the sub-category instead.
            updatedEntries.push({
                ...entry,
                rules: [{
                    startDate: dayjs().year(newYear).startOf('year').toDate(),
                    amount: 0,
                }],
            });
        } else {
            updatedEntries.push({
                ...entry,
                rules: updatedRules,
            });
        }
    }

    return updatedEntries;
});

budgetSchema.static('get', async function get(userId: string, year: number, ignoreHidden = false) {
    let query: FilterQuery<HydratedBudget> = {
        year,
    };
    if (ignoreHidden) {
        query.isHidden = { $ne: true };
    }
    return this.findOne(query).forUser<HydratedBudget>(userId);
});

budgetSchema.static('getClosest', async function getClosest(userId: string, year: number, ignoreHidden = false) {
    let query: FilterQuery<HydratedBudget> = { userId };
    if (ignoreHidden) {
        query.isHidden = { $ne: true };
    }
    const budgets = await this.find(query).sort('year').select('year');
    const budgetedYears = budgets.map((budget) => budget.year);
    if (isEmpty(budgets)) {
        return null;
    }

    const earlierBudgetedYear = min(budgetedYears);
    if (earlierBudgetedYear > year) {
        return this.findOne({ year: earlierBudgetedYear }).forUser<HydratedBudget>(userId);
    } else {
        return this.findOne({ year: findLast(budgetedYears, (budgetedYear) => budgetedYear <= year) }).forUser<HydratedBudget>(userId);
    }
});

budgetSchema.static('toDto', function toDto(
    budget: LeanBudget,
    rootCategories: CategoryDto[]
): BudgetDto {
    return {
        id: budget?._id?.toString() || budget.id,
        isClosed: budget?.isClosed,
        year: budget?.year,
        hasBudget: !!budget,
        budgetEntries: budget?.entries?.map((entry) => entry.toDto()) || [],
        rootCategories,
    };
});

budgetSchema.static('getActive', async function getActive(userId: string): Promise<HydratedBudget> {
    return this.findOne({ categoriesSnapshot: [] }).forUser(userId);
});

export { budgetSchema };
export const Budget = model<BudgetDocument, BudgetModel>('budget', budgetSchema);
