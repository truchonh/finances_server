const _ = require('lodash');
const AbstractMigrationScript = require('../AbstractMigrationScript');
const { Transaction } = require('../../transactionCollection');
const { updatePlannedTransactions } = require('../../../_resources_/budget/budgetController');

class MatchPlannedTransactions extends AbstractMigrationScript {
    static async migrate() {
        const unMatchedTransactions = await Transaction.find({
            date: { $gte: new Date(2023, 10, 7) },
        });
        const groupedByUser = _.groupBy(unMatchedTransactions, (t) => t.userId);
        for (const [userId, transactions] of Object.entries(groupedByUser)) {
            await updatePlannedTransactions(userId, transactions);
        }
    }
}
module.exports = MatchPlannedTransactions;
