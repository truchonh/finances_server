const AbstractMigrationScript = require('../AbstractMigrationScript');
const { User } = require('../../userCollection');

class ClearingRegisteredDevices extends AbstractMigrationScript {
    static async migrate() {
        for (let user of await User.find()) {
            user.devices = [];
            await user.save();
        }
    }
}
module.exports = ClearingRegisteredDevices;
