const AbstractMigrationScript = require('../AbstractMigrationScript');
const { Budget } = require('../../budgetCollection');
const { replacePlannedTransactions } = require('../../../_resources_/budget/budgetController');

class RemapPlannedTransactions extends AbstractMigrationScript {
    static async migrate() {
        const budgets = await Budget.find().exec();
        for (const budget of budgets) {
            for (const entry of budget.entries) {
                await replacePlannedTransactions(budget.year, entry);
            }
        }
    }
}
module.exports = RemapPlannedTransactions;
