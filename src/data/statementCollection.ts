/**
 * Created by hugo on 2018-05-13.
 */
import { extend, isArray, isEmpty } from 'lodash';
import { Schema, Model, model, Document, HydratedDocument, FilterQuery, LeanDocument } from 'mongoose';
import { forUser, GenericQueryHelpers, GenericSchema, IGenericSchema } from './genericModel';
import { Statement as StatementDto } from '../models/entities';
import { StatementFileType } from '../models/enums';
import AggregationBuilder from '../models/AggregationBuilder';

export interface IStatement extends IGenericSchema {
    date: Date;
    creationDate: Date;
    fileId?: string;
    fileType: StatementFileType;
    isApproved?: boolean;
    userId: string;
}

export interface IStatementMethods {
    toDto(): StatementDto;
}

interface StatementDocument extends IStatement, Document, IStatementMethods {}
export type LeanStatement = LeanDocument<StatementDocument>;
export type HydratedStatement = HydratedDocument<StatementDocument>;

interface StatementModel extends Model<StatementDocument, GenericQueryHelpers<StatementDocument>, IStatementMethods> {
    aggregateTransactionCount(
        userId: string,
        size: number,
        skip: number
    ): Promise<Array<StatementDocument & { transactionCount: number; lastTransactionDate: Date }>>;
    searchForUser(
        userId: string,
        size: number,
        skip: number,
        filter: Record<string, any>,
        sort: Record<string, 1 | -1>
    ): Promise<StatementDocument[]>;
}

let statementSchema = new Schema<
    StatementDocument,
    StatementModel,
    IStatementMethods,
    GenericQueryHelpers<StatementDocument>
>(
    extend(
        {
            date: { type: Schema.Types.Date, required: true },
            creationDate: { type: Schema.Types.Date, default: Date.now },
            fileId: String,
            fileType: {
                type: Number,
                enum: [
                    StatementFileType.visa,
                    StatementFileType.desjardins,
                    StatementFileType.paypal,
                    StatementFileType.webScraper,
                ],
                required: true,
            },
            isApproved: { type: Boolean, default: false },
            userId: String,
        },
        GenericSchema
    )
);

statementSchema.query.forUser = forUser<StatementDocument>;

statementSchema.method('toDto', function toDto(this: StatementDocument): StatementDto {
    return {
        id: this._id?.toString() || this.id,
        date: this.date,
        fileType: this.fileType,
        fileId: this.fileId,
        isApproved: this.isApproved,
        creationDate: this.creationDate,
    };
});

statementSchema.static('aggregateTransactionCount', async function aggregateTransactionCount(
    userId: string,
    size: number,
    skip: number
) {
    const builder = new AggregationBuilder(this);
    return builder
        .match({ userId, isApproved: true })
        .sort({ date: -1, _id: 1 })
        .skip(skip)
        .limit(size)
        .lookup({
            from: 'transactions',
            localField: '_id',
            foreignField: 'statementId',
            as: 'transactions',
        })
        .project({
            transactionCount: { $size: '$transactions' },
            lastTransactionDate: { $max: '$transactions.date' },
            isApproved: 1,
            date: 1,
            fileType: 1,
            fileId: 1,
            creationDate: 1,
            userId: 1,
            _id: 1,
        })
        .commit();
});

statementSchema.static('searchForUser', async function searchForUser(
    userId: string,
    size: number,
    skip: number,
    filter: Record<string, any>,
    sort: Record<string, 1 | -1>
) {
    // TODO: Lots of copy paste from the Transaction.searchForUser function !!!
    const allowedFields = ['fileType', 'date', 'isApproved', 'creationDate'];

    const query: FilterQuery<any> = {
        userId,
    };
    // parse the filter object
    if (!isEmpty(filter)) {
        for (const [field, value] of Object.entries(filter)) {
            if (field === 'date_from') {
                query['date'] = {
                    ...query['date'],
                    $gte: new Date(value),
                };
            } else if (field === 'date_to') {
                query['date'] = {
                    ...query['date'],
                    $lt: new Date(value),
                };
            } else if (field === 'date') {
                query[field] = new Date(value);
            } else if (allowedFields.includes(field)) {
                query[field] = isArray(value) ? { $in: value } : value;
            }
        }
    }

    const sortQuery: Record<string, 1 | -1> = {};
    if (!isEmpty(sort)) {
        for (const [field, direction] of Object.entries(sort)) {
            if (allowedFields.includes(field)) {
                sortQuery[field] = direction;
            }
        }
    }
    sortQuery['_id'] = 1;

    const builder = new AggregationBuilder(this);
    return builder
        .match(query)
        .sort(sortQuery)
        .skip(skip)
        .limit(size)
        .commit();
});

export { statementSchema };
export const Statement = model<StatementDocument, StatementModel>('statement', statementSchema);
