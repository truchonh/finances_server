/**
 * Created by hugo on 2018-05-13.
 */

import { extend, flatten, isEmpty, uniq } from 'lodash';
import {Schema, Model, LeanDocument, model, Document, HydratedDocument, QueryWithHelpers} from 'mongoose';
import { CategoryType, SystemCategory } from '../models/enums';
import { forUser, IGenericSchema, GenericQueryHelpers, GenericSchema } from './genericModel';
import { Category as CategoryDto } from '../models/entities';

export interface ICategory extends IGenericSchema {
    name: string;
    creationDate: Date;
    type: CategoryType;
    systemType: SystemCategory;
    hue?: string;
    parent?: string;
    matchedDescriptions?: Array<string>;
}

interface ICategoryMethods {
    toDto(): CategoryDto;
}

type CategoryDocument = ICategory & Document & ICategoryMethods;
export type HydratedCategory = HydratedDocument<CategoryDocument>;
export type LeanCategory = LeanDocument<CategoryDocument>;

interface CategoryQueryHelper extends GenericQueryHelpers<CategoryDocument> {
    ofBaseType(systemType?: SystemCategory): CategoryQueryHelper;
}

interface CategoryModel extends Model<CategoryDocument, CategoryQueryHelper, ICategoryMethods> {
    findChildBaseCategories(
        userId: string,
        parentName: string,
        categories?: Array<LeanCategory>
    ): Promise<Array<Partial<ICategory>>>;
    findSubCategories(userId: string, parentName: string): Promise<Array<ICategory>>;
    createIfNotExistsOrUpdate(
        userId: string,
        type: CategoryType,
        categoryName: string,
        systemType: SystemCategory,
        matchedDescriptions?: Array<string>,
        parent?: string,
    ): Promise<void>;
    toDto(category: CategoryDto | LeanCategory): CategoryDto;
    getTransferCategoryNames(userId: string): Promise<Array<string>>;
}

let categorySchema = new Schema<
    CategoryDocument,
    CategoryModel,
    ICategoryMethods,
    CategoryQueryHelper
>(
    extend(
        {
            name: { type: String, required: true },
            creationDate: { type: Schema.Types.Date, default: Date.now },
            type: { type: Number, enum: [CategoryType.base, CategoryType.group], required: true },
            systemType: {
                type: String,
                enum: [SystemCategory.goal, SystemCategory.income, SystemCategory.transfer, SystemCategory.expense],
                required: true,
            },
            hue: { type: String },
            parent: String,
            matchedDescriptions: [String],
        },
        GenericSchema
    ),
    { collection: 'categories' }
);
categorySchema.index({ name: 1, userId: 1, parent: 1 });

categorySchema.query.forUser = forUser<CategoryDocument>;
categorySchema.query.ofBaseType = function ofBaseType(
    this: QueryWithHelpers<CategoryDocument, HydratedCategory, CategoryQueryHelper>,
    systemType?: SystemCategory
) {
    if (systemType === undefined) {
        return this.where({ type: CategoryType.base });
    } else {
        return this.where({ type: CategoryType.base, systemType });
    }
};

categorySchema.method('toDto', function toDto(): CategoryDto {
    return Category.toDto(this);
});

categorySchema.static('findChildBaseCategories', async function findChildBaseCategories(
    userId: string,
    parentName: string,
    categories?: Array<LeanCategory>
) {
    const allCategories = categories || (await this.find().forUser(userId).select('name parent type').lean());

    const search = (parentName: string) => {
        const subCategories = allCategories.filter((c) => c.parent === parentName);
        const subCatArrays = [[parentName]];
        for (let subCat of subCategories) {
            subCatArrays.push(search(subCat.name));
        }
        return flatten(subCatArrays);
    };

    const subCategoryNames = search(parentName);
    return allCategories.filter((_cat) => _cat.type === CategoryType.base && subCategoryNames.includes(_cat.name));
});

categorySchema.static('findSubCategories', async function findSubCategories(userId: string, parentName: string) {
    const allCategories = await this.find().forUser(userId).lean();
    const search = (parent: ICategory, ignoreSelf = false) => {
        let subCategories = allCategories.filter((c) => c.parent === parent.name);
        let subCatArrays = ignoreSelf ? [] : [parent];
        for (let subCat of subCategories) {
            subCatArrays.push(...search(subCat));
        }
        return flatten(subCatArrays);
    };
    return search(
        allCategories.find((_cat) => _cat.name === parentName),
        true
    );
});

categorySchema.static('createIfNotExistsOrUpdate', async function createIfNotExistsOrUpdate(
    userId: string,
    type: CategoryType,
    categoryName: string,
    systemType: SystemCategory,
    matchedDescriptions?: Array<string>,
    parent?: string,
) {
    let category = await this.findOne({
        name: categoryName,
    }).forUser<HydratedCategory>(userId);
    if (category) {
        if (!isEmpty(matchedDescriptions) && category.type === CategoryType.base) {
            category.matchedDescriptions = uniq([
                ...(category.matchedDescriptions || []),
                ...matchedDescriptions
            ]);
            await category.save();
        }
    } else {
        await this.insertMany([
            {
                userId: userId,
                name: categoryName,
                type: type,
                systemType: systemType,
                matchedDescriptions: isEmpty(matchedDescriptions) ? undefined : matchedDescriptions,
                parent: parent || null,
            },
        ]);
    }
});

categorySchema.static('toDto', function toDto(category: CategoryDto | LeanCategory): CategoryDto {
    return {
        id: (category as LeanCategory)._id?.toString() || category.id,
        type: category.type,
        name: category.name,
        systemType: category.systemType,
        hue: category.hue,
        parent: category.parent,
        creationDate: category.creationDate,
        children: (category as CategoryDto).children || null,
        matchedDescriptions: category.matchedDescriptions || null,
    };
});

categorySchema.static('getTransferCategoryNames', async function getTransferCategoryNames(userId: string) {
    let excludedCategories = await Category.find()
        .ofBaseType(SystemCategory.transfer)
        .forUser(userId)
        .select('name')
        .lean();
    return excludedCategories.map(c => c.name);
})

export { categorySchema };
export const Category = model<CategoryDocument, CategoryModel>('category', categorySchema);
