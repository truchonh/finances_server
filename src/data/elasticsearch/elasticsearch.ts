/**
 * Created by hugo on 2018-05-13.
 */
import { TransactionDescriptionInfo, TransactionEsEntity } from '../../models/entities';
import { Client } from 'es7';
import * as _ from 'lodash';
import { SearchHit, SearchResponse } from 'es7/api/types';
import * as logger from '../../utils/simpleLogger';
import { mostRelevantCategory, userSearch } from './esRequestBuilder';
import { HydratedTransaction, Transaction } from '../transactionCollection';
import * as config from 'config';

const _client = new Client({
    node: config.get('DATA.ELASTICSEARCH_HOST'),
});

export async function untilElasticsearchConnection() {
    try {
        await _client.ping();
    } catch (err) {
        console.log('Could not connect to the Elasticsearch service. Will retry in 5 seconds.');

        let retries;
        for (retries = 0; retries < 10; retries++) {
            try {
                console.log(`Elasticsearch connection retry #${retries}...`);

                await _client.ping();
                return;
            } catch (_) {
                console.log('Could not connect to the Elasticsearch service. Will retry in 5 seconds.');
            }

            await new Promise((resolve) => setTimeout(resolve, 5000));
        }

        if (retries === 10) {
            throw err;
        }
    }
}

export async function esExists() {
    return _client.indices.exists({
        index: config.get('INSTANCE'),
    });
}

export async function esInitialize() {
    return _client.indices.create({
        index: config.get('INSTANCE'),
        body: {
            settings: config.get('ELASTICSEARCH.settings'),
            mappings: config.get('ELASTICSEARCH.mappings'),
        },
    });
}

type CategoryMatch = {
    transactionId: string;
    score: number;
    descriptionInfo: TransactionDescriptionInfo | string;
    category: string;
};
export async function createCategoryGuesstimator(
    userId: string,
    transactions: Array<{ descriptionInfo?: TransactionDescriptionInfo; description?: string }>,
    fileType?: number
) {
    const uniqueDescriptions = _.chain(transactions)
        .map((_record) => _record.descriptionInfo || _record.description)
        .uniqBy((_info) => JSON.stringify(_info))
        .value();
    if (_.isEmpty(uniqueDescriptions)) {
        return;
    }

    let body: any[] = [];

    uniqueDescriptions.forEach((info) => {
        body.push({}, mostRelevantCategory(userId, info, fileType));
    });

    let res = await _client.msearch({
        index: config.get('INSTANCE'),
        body,
    });
    let results: CategoryMatch[] = [];
    res.body.responses.forEach((r: SearchResponse<TransactionEsEntity>, i: number) => {
        if (r?.hits?.hits?.length > 0) {
            let firstHit = r.hits.hits[0];
            results.push({
                transactionId: firstHit._id?.toString(),
                score: firstHit._score,
                descriptionInfo: uniqueDescriptions[i],
                category: firstHit._source && firstHit._source.category,
            });
        }
    });

    return {
        find(descriptionInfo: TransactionDescriptionInfo | string) {
            return results.find((result) => _.isEqual(descriptionInfo, result.descriptionInfo));
        },
    };
}

export async function getRelevantCategories(
    userId: string,
    descriptionInfo: TransactionDescriptionInfo | string,
    fileType?: number
) {
    const response = await _client.search({
        index: config.get('INSTANCE'),
        size: 200,
        body: mostRelevantCategory(userId, descriptionInfo, fileType),
    });
    const hits: Array<{ score: number; value: string }> =
        response?.body?.hits?.hits?.map((item: SearchHit<TransactionEsEntity>) => ({
            score: item._score,
            value: item._source.category,
        })) || [];
    return _.uniqBy(hits, 'value');
}

export async function esSearch(userId: string, query: string, size: number, skip: number, from: Date, to: Date) {
    let transactionIds = [];

    try {
        let response = await _client.search({
            index: config.get('INSTANCE'),
            size: size,
            from: skip,
            body: userSearch(userId, query, from, to),
        });

        if (response?.body?.hits?.hits?.length > 0) {
            for (let hit of response.body.hits.hits) {
                transactionIds.push(hit._id);
            }
        }
    } catch (err) {
        logger.warning(`user search error: ${err.message}`);
    }

    return transactionIds;
}

export async function indexTransactions(transactions: HydratedTransaction[]) {
    let body: any[] = [];
    transactions.forEach((t) => {
        body.push({
            index: { _id: t.id },
        });
        let esTransaction = t.toEsEntity();
        body.push(esTransaction);
    });
    await _client.bulk({
        _source: [],
        index: config.get('INSTANCE'),
        body: body,
    });
}

export async function upsertEsTransactions(transactions: TransactionEsEntity[]) {
    let body: any[] = [];
    transactions.forEach((t) => {
        body.push({
            update: { _id: t.id },
        });
        body.push({
            doc: t,
            doc_as_upsert: true,
        });
    });
    if (_.isEmpty(body)) {
        return;
    }
    await _client.bulk({
        _source: [],
        index: config.get('INSTANCE'),
        body,
    });
}

export async function deleteTransactionsByStatement(statementId: string) {
    await _client.deleteByQuery({
        index: config.get('INSTANCE'),
        body: {
            query: {
                term: { statement: statementId },
            },
        },
    });
}

export async function deleteEsTransaction(transactionId: string) {
    await _client.delete({
        id: transactionId,
        index: config.get('INSTANCE'),
    });
}

export async function deleteTransactionsById(transactionIds: string[]) {
    const body = transactionIds.map((id) => ({ delete: { _id: id } }));
    if (_.isEmpty(body)) {
        return;
    }
    await _client.bulk({
        _source: [],
        index: config.get('INSTANCE'),
        body,
    });
}

export async function deleteIndex() {
    return _client.indices.delete({
        index: config.get('INSTANCE'),
    });
}

export async function reindex() {
    console.log(`Reindexing the Elasticsearch database...`);

    const batchSize = 1000;

    await deleteIndex();
    await esInitialize();

    for (let fileType = 0; fileType < 4; fileType++) {
        let transactions = await Transaction.getWithCategoryHierarchy({
            isPending: { $ne: true },
        });
        const transactionEsEntities: any[] = transactions.map((t) => {
            let sortedCategories = _.sortBy(t.category_hierarchy, 'depth').reverse();
            return {
                ...t,
                categories: sortedCategories.map((_cat) => _cat.name.toLowerCase()),
            };
        });

        let batchCount = Math.ceil(transactionEsEntities.length / batchSize);
        for (let i = 0; i < batchCount; i++) {
            let transactionBatch = transactionEsEntities.slice(i * batchSize, (i + 1) * batchSize);

            let body: any[] = [];
            transactionBatch.forEach((_transaction) => {
                body.push({
                    index: { _id: _transaction._id.toString() },
                });

                _transaction._id = null;
                _transaction.category_hierarchy = null;

                _transaction.description = _transaction.description?.toLowerCase() || null;
                _transaction.type = _transaction.descriptionInfo?.type?.toLowerCase() || null;
                _transaction.location = _transaction.descriptionInfo?.location?.toLowerCase() || null;
                _transaction.descriptionMain = _transaction.descriptionInfo?.main?.toLowerCase() || null;
                _transaction.descriptionInfo = null;

                _transaction.category = _transaction.category && _transaction.category.toLowerCase();
                _transaction.accountType = fileType;

                // remove empty fields
                for (let key in _transaction) {
                    if (_transaction.hasOwnProperty(key)) {
                        if (_transaction[key] === null || _transaction[key] === undefined || _transaction[key] === '') {
                            delete _transaction[key];
                        }
                    }
                }

                body.push(_transaction);
            });
            await _client.bulk({
                _source: [],
                index: config.get('INSTANCE'),
                body: body,
            });
        }
    }

    console.log(`Reindex done !`);
}
