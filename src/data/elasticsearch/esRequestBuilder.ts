/**
 * Created by hugo on 2018-05-13.
 */
import { TransactionDescriptionInfo } from '../../models/entities';

export function mostRelevantCategory(
    userId: string,
    descriptionInfo: string | TransactionDescriptionInfo,
    fileType: number
) {
    let body: any = {
        query: {
            bool: {
                filter: [
                    {
                        exists: { field: 'category' },
                    },
                    {
                        term: {
                            userId: userId,
                        },
                    },
                ],
                must: [
                    typeof descriptionInfo === 'string'
                        ? {
                              multi_match: {
                                  query: descriptionInfo.toLowerCase(),
                                  fields: ['description^5', 'category'],
                                  fuzziness: 'auto',
                              },
                          }
                        : {
                              multi_match: {
                                  query: descriptionInfo.main.toLowerCase().trim(),
                                  fields: ['descriptionMain^5'],
                                  fuzziness: 'auto',
                              },
                          },
                ],
                must_not: [
                    {
                        term: {
                            ignoreForAutoCategoryAssignment: true,
                        },
                    },
                ],
                should: [],
            },
        },
    };

    if (typeof descriptionInfo === 'object') {
        if (descriptionInfo.type) {
            body.query.bool.must.push({
                match: {
                    type: descriptionInfo.type.toLowerCase().trim(),
                },
            });
        }
        if (descriptionInfo.location) {
            body.query.bool.should.push({
                match: {
                    location: descriptionInfo.location.toLowerCase().trim(),
                },
            });
        }
    }

    if (fileType) {
        body.query.bool.should.push({
            term: {
                accountType: {
                    value: fileType,
                },
            },
        });
    }

    return body;
}

export function userSearch(userId: string, rawQuery: string, from: Date, to: Date) {
    let query: any = {
        query: {
            bool: {
                filter: [
                    {
                        term: {
                            userId: userId,
                        },
                    },
                ],
                must: [
                    {
                        query_string: {
                            query: rawQuery,
                            fields: ['description^5', 'description.ngram^3', 'category^3', 'category.ngram^2', 'type'],
                        },
                    },
                ],
            },
        },
    };

    if (from && to) {
        query.query.bool.filter = [
            {
                range: {
                    date: {
                        gte: from.getTime(),
                        lte: to.getTime(),
                    },
                },
            },
        ];
    }

    return query;
}
