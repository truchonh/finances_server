import * as _ from 'lodash';

type Coordinate = {
    x: number;
    y: number;
    id: string;
};

type NearestNeighbourParams = {
    distanceThreshold: number;
};

export function pairCoordinatesByNearestNeighbour(
    referenceCoordinates: Coordinate[],
    matchingCoordinates: Coordinate[],
    params?: NearestNeighbourParams
): Map<string, string> {
    const calculatedThreshold = params?.distanceThreshold ? params!.distanceThreshold ** 2 : Number.MAX_SAFE_INTEGER;

    const distanceMatrix: Record<string, Record<string, number>> = {};
    for (let refCoordinate of referenceCoordinates) {
        distanceMatrix[refCoordinate.id] = {};
        for (let matchCoordinate of matchingCoordinates) {
            // fast euclidean distance (https://en.wikibooks.org/wiki/Algorithms/Distance_approximations)
            // calculated without square root, used for comparison only (distance value is meaningless here)
            const distance = (matchCoordinate.x - refCoordinate.x) ** 2 + (matchCoordinate.y - refCoordinate.y) ** 2;
            distanceMatrix[refCoordinate.id][matchCoordinate.id] = distance < calculatedThreshold ? distance : null;
        }
    }

    const finalMapping = new Map();
    const reverseMap: Record<string, string[]> = {};

    for (let [refId, row] of Object.entries(distanceMatrix)) {
        const matchId = _.chain(Object.entries(row))
            .filter(([matchId, distance]) => distance !== null)
            .sortBy(([matchId, distance]) => distance)
            .map(([matchId]) => matchId)
            .first()
            .value();
        finalMapping.set(refId, matchId || null);
        reverseMap[matchId] = [...(reverseMap[matchId] || []), refId];
    }

    // Check the reverse map for double matches.
    for (let [matchId, refIds] of Object.entries(reverseMap)) {
        if (refIds.length <= 1) {
            continue;
        }
        // This transaction was matched with multiple references.
        // Find the one with the shortest distance..
        const closestRefId = _.chain(refIds)
            .map((refId) => {
                const distance = distanceMatrix[refId][matchId];
                return [refId, distance];
            })
            .sortBy(([refId, distance]) => distance)
            .map(([refId]) => refId)
            .first()
            .value();
        // .. and remove all other matches form the mapping.
        for (const refId of refIds) {
            if (refId !== closestRefId) {
                finalMapping.set(refId, null);
            }
        }
    }
    return finalMapping;
}
