/**
 * Created by hugo on 2018-05-13.
 */
import { lookup } from 'geoip-lite';
import * as logger from './simpleLogger';
import { Request, Response } from 'express';

const ipware = require('ipware');
const getIp = ipware().get_ip as (request: Request) => { clientIp: string; clientIpRoutable: boolean };

/**
 * Handle an internal server error
 */
export function handle500(req: Request, res: Response, err: Error) {
    logger.error(err);
    let stack: string[] = [];
    err.stack.split('\n').forEach((line) => {
        stack.push(line);
    });
    res.status(500).send({
        url: req.originalUrl,
        message: err.message,
        internalError: stack,
    });
}

/**
 * Handle generic request error.
 * Server side validation can return this type of error.
 */
export function handle4XX(req: Request, res: Response, message: string, status = 400, notify = false) {
    if (notify) {
        const ip = getIp(req).clientIp;
        let locationStr = '';
        if (process.env.NODE_ENV === 'PROD') {
            const location = lookup(ip);
            if (location) {
                locationStr = `${location.city}, ${location.region}, ${location.country}`;
            } else {
                locationStr = 'local';
            }
            logger.log(`[geoip data] ${ip} - ${locationStr} - ${req.originalUrl}`);
        }
    }
    res.status(status).send({
        url: req.originalUrl,
        status: status,
        message: message || null,
    });
}

/**
 * Handle a non-authorized error
 */
export function handle401(req: Request, res: Response, message = 'Accès non authorisé à la ressource.') {
    handle4XX(req, res, message, 401);
}

/**
 * handle a forbidden error
 */
export function handle403(req: Request, res: Response, err: Error) {
    handle4XX(req, res, JSON.stringify({ message: err.message, stack: err.stack }, null, 2), 403, true);
}

/**
 * Handle a not found error
 */
export function handle404(req: Request, res: Response, err: Error) {
    handle4XX(req, res, err && JSON.stringify({ message: err.message, stack: err.stack }, null, 2), 404);
}
