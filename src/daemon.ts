import * as logger from './utils/simpleLogger';
import * as fs from 'fs';
import * as dayjs from 'dayjs';
import { CronJob as Cron } from 'cron';
import { fork } from 'child_process';
import { BudgetModelBuilder } from './models/BudgetModelBuilder';
import { Budget } from './data/budgetCollection';
import { reindex } from './data/elasticsearch/elasticsearch';
import { reloadData } from 'geoip-lite';

let secrets: { maxmind_key?: string } = {};
if (process.env.API_CONFIG_PATH) {
    const secretData = fs.readFileSync(process.env.API_CONFIG_PATH);
    secrets = JSON.parse(secretData.toString());
}

export async function startDaemon() {
    // every day @ 5am
    let reindexJob = new Cron('0 5 * * *', () => reindex());
    reindexJob.start();

    // every sunday @ 3am
    let backupJob = new Cron('0 3 * * 0', () => backupDatabase());
    backupJob.start();

    if (secrets.maxmind_key) {
        // every sunday @ 4am
        let geoipUpdateJob = new Cron('0 4 * * 0', () => updateGeoIpData());
        geoipUpdateJob.start();
    } else {
        logger.warning('Missing "maxmind_key". GeoIP update job disabled.');
    }

    // first day of each month
    let setMonthlyBudgetPlannedDataJob = new Cron('0 0 1 * *', () => saveMonthlyBudgetPlannedPlotData());
    setMonthlyBudgetPlannedDataJob.start();
}

function backupDatabase() {
    forkScript('./scripts/backup.js', null, (exitCode, signal) => {
        if (exitCode && exitCode !== 0) {
            // log the error
            logger.error('backup script exited with code (stderr output above): ' + exitCode);
            return;
        } else if (signal) {
            logger.error('backup script was terminated by signal (stderr output above): ' + signal);
            return;
        }
        console.log('Backup script executed successfully !');
    });
}

function updateGeoIpData() {
    forkScript(
        './node_modules/geoip-lite/scripts/updatedb.js',
        [`license_key=${secrets.maxmind_key}`],
        (exitCode, signal) => {
            if (exitCode && exitCode !== 0) {
                // log the error
                logger.error('geoip updatedb process exited with code: ' + exitCode);
                return;
            } else if (signal) {
                logger.error('geoip updatedb process was terminated by signal: ' + signal);
                return;
            }
            reloadData(() => console.log('geoip-lite database reloaded successfully'));
        }
    );
}

function forkScript(scriptPath: string, args: string[], cb: (code: number, signal: NodeJS.Signals) => void) {
    const scriptName = scriptPath.split('/').pop();
    let backupProcess = fork(scriptPath, args, { stdio: 'pipe' });

    backupProcess.stdout.on('data', (data) => console.log(`[${scriptName}] ${data}`));
    backupProcess.stderr.on('data', (data) => logger.error(`[${scriptName}] ${data}`));
    backupProcess.on('exit', cb);
}

async function saveMonthlyBudgetPlannedPlotData() {
    const currentBudgets = await Budget.find({ year: dayjs().year() });
    for (let budget of currentBudgets) {
        if (budget.plannedBudgetSumHistory.find((_entry) => _entry.month === dayjs().month())) {
            continue;
        }

        const from = dayjs().year(budget.year).startOf('year');
        const to = from.clone().endOf('year');
        const daysInRange = to.diff(from, 'day', true);
        const budgetModel = await BudgetModelBuilder.create({
            userId: budget.userId,
            modelYear: budget.year,
            from,
            to,
        })
            .appendPlannedAmount()
            .build();

        budget.plannedBudgetSumHistory.push({
            month: dayjs().month(),
            value: daysInRange * budgetModel.getGlobalPlannedSumParDay(),
        });

        budget.markModified('plannedBudgetSumHistory');
        await budget.save();
    }
}
