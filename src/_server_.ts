/**
 * Created by hugo on 2018-05-13.
 */
import * as path from 'path';
import * as fs from 'fs';
import * as dayjs from 'dayjs';
import * as logger from './utils/simpleLogger';
import './data/_database_';
import { startApiWorker } from './worker';
import { startDaemon } from './daemon';
import { initializeData, untilDatabaseConnection } from './data/_database_';
import { untilElasticsearchConnection } from './data/elasticsearch/elasticsearch';
import paths from '../config/paths';

const isProd = process.env.NODE_ENV === 'PROD';

process
    .on('unhandledRejection', (reason: any) => {
        logger.error(`unhandledRejection: ${reason}`);
    })
    .on('uncaughtException', (err) => {
        logger.error(`Uncaught Exception thrown: ${err}`);
    });

async function start() {
    const fsCompatibleDate = dayjs().format('YYYY-MM-DD[_]HH.mm.ss.SSS[_]ZZ');

    if (isProd) {
        const access = fs.createWriteStream(path.join(paths.SYSTEM_LOGS, `${fsCompatibleDate}__system.txt`), {
            flags: 'a',
        });
        process.stdout.write = process.stderr.write = access.write.bind(access);
        process.on('uncaughtException', function (err) {
            logger.error(err);
        });
    }

    await Promise.all([untilDatabaseConnection(), untilElasticsearchConnection()]);

    await initializeData();
    await startApiWorker();

    if (isProd) {
        await startDaemon();
    }

    if (process.env.NODE_ENV === 'DEV') {
        require('../../client/server');
    }
}
start().catch((err) => {
    logger.error(err);
    process.exit(-1);
});
