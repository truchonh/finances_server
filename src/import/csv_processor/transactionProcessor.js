/**
 * Created by hugo on 2018-05-17.
 */

const fs = require('fs/promises');
const Papa = require('papaparse');

const desjardinsProcessor = require('./desjardinsProcessor');
const visaProcessor = require('./visaProcessor');
const paypalProcessor = require('./paypalProcessor');
const importConfig = require('config').get('IMPORT');
const { StatementFileType } = require('../../models/enums');

module.exports = class {
    /**
     * Process a PDF statement into transactions
     * @param fileType {number}
     * @param fileStream {ReadableStream}
     * @returns {Promise<Transaction[]>}
     */
    static async extract(fileType, fileStream) {
        const data = await this._getData(fileType, fileStream);
        switch (fileType) {
            case StatementFileType.desjardins:
                return desjardinsProcessor.process(data);
            case StatementFileType.visa:
                return visaProcessor.process(data);
            case StatementFileType.paypal:
                return paypalProcessor.process(data);
        }
    }

    /**
     * Extract the most recent balance for each account in the statement
     * @param fileType
     * @param fileStream
     * @returns {Promise<[]|*>}
     */
    static async getBalance(fileType, fileStream) {
        const data = await this._getData(fileType, fileStream);
        switch (fileType) {
            case StatementFileType.desjardins:
                return desjardinsProcessor.getBalance(data);
            case StatementFileType.visa:
                return visaProcessor.getBalance(data);
            case StatementFileType.paypal:
                return paypalProcessor.getBalance(data);
        }
    }

    static async _getData(fileType, fileStream) {
        let lines;
        let result;
        let encoding = importConfig.parsing_encoding;
        if (fileType === StatementFileType.paypal) {
            encoding = 'utf-8';
        }

        lines = await fs.readFile(fileStream, encoding);
        result = Papa.parse(lines, {
            delimiter: importConfig.parsing_delimiter,
            dynamicTyping: true,
        });
        return result.data;
    }
};
