/**
 * Created by Hugo on 2018-07-13
 */

const fs = require('fs');
const { StatementFileType } = require('../../models/enums');
const pdfHelper = require('./searchablePdfHelper');
const visaPdfProcessor = require('./visa');
const desjardinsPdfProcessor = require('./desjardins');

module.exports = class {
    /**
     * Process raw data from a PDF statement into transactions.
     * @param fileType {Number}
     * @param fileStream {ReadableStream}
     * @returns {Promise<Transaction[]>}
     */
    static async extract(fileType, fileStream) {
        if (fileType !== StatementFileType.visa && fileType !== StatementFileType.desjardins)
            throw new Error(`Unsupported statement file type. (${fileType})`);

        let fileData = await pdfHelper.extractPdfData(fileStream);

        // Keep raw text in case of exception.
        const tempFilePath = `./logs/pdf-statement-dump_${Date.now()}.txt`;
        fs.writeFileSync(tempFilePath, fileData.text_pages.join('\n\n\n'), 'utf-8');

        let transactions;
        switch (fileType) {
            case StatementFileType.visa:
                transactions = visaPdfProcessor.process(fileData.text_pages);
                break;
            case StatementFileType.desjardins:
                transactions = desjardinsPdfProcessor.process(fileData.text_pages);
                break;
            default:
                throw new Error('Unsupported statement file type.');
        }

        // Everything went well. Delete temp text file.
        fs.unlinkSync(tempFilePath);

        return transactions;
    }

    static async getBalance(fileType, fileStream) {
        if (fileType !== StatementFileType.visa && fileType !== StatementFileType.desjardins)
            throw new Error(`Unsupported statement file type. (${fileType})`);

        let fileData = await pdfHelper.extractPdfData(fileStream);

        switch (fileType) {
            case StatementFileType.visa:
                return visaPdfProcessor.getBalance(fileData.text_pages);
            case StatementFileType.desjardins:
                return desjardinsPdfProcessor.getBalance(fileData.text_pages);
            default:
                throw new Error('Unsupported statement file type.');
        }
    }
};
