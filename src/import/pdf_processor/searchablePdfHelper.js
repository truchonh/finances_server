const importHelper = require('../importHelper');
const service = require('../../services/service.ts');
const { parseAccountType } = require('../../models/enums');

class searchablePdfHelper {
    /**
     * Extract text from searchable PDF.
     * @param fileStream {ReadableStream}
     * @returns {Promise<Object>}
     */
    static async extractPdfData(fileStream) {
        let response = await service.pdfExtract.extract(fileStream);
        return response.body;
    }

    static splitIntoLines(textPage) {
        let lines = textPage.split('\r\n');
        if (lines.length === 1) lines = textPage.split('\n');
        return lines;
    }

    /**
     * Check if a line contains the account type header from a Desjardins statement.
     * @param line {String}
     * @returns {boolean}
     */
    static isLineAccountTypeHeader(line) {
        let wordGroups = this.findWordGroups(line, 3);
        // account description line is expected to have two word group: type and full name.
        // account identifier should have 5 characters or less (ex: 'ET 1', 'MC 12', 'EOP')
        return wordGroups.length === 2 && wordGroups[0].length <= 5 && parseAccountType(wordGroups[0]);
    }

    /**
     * Test if this line look like the first for a transaction.
     * @param line {String}
     * @returns {boolean}
     */
    static isFirstLineOfGroup(line) {
        let words = this.findWords(line);
        let dayNumber = parseInt(words[0]);
        return (
            words.length >= 2 &&
            !Number.isNaN(dayNumber) &&
            dayNumber > 0 &&
            dayNumber < 32 &&
            importHelper.shortMonthStringToInt(words[1]) !== -1
        );
    }

    /**
     * Find amounts from a Desjardins transaction line.
     * @param line {String}
     * @returns {Number[]}
     */
    static findAmountsWithinLine(line) {
        const amountRegex = /^[0-9]*(\.[0-9][0-9])-?$/;

        return this.findWordGroups(line, 2)
            .map((g) => this.findWords(g).join(''))
            .filter((g) => amountRegex.test(g))
            .map((s) => importHelper.parseDesjardinsAmount(s)); // keep absolute value, sign is inverted depending
        //                                                         if the balance is increased of decreased.
    }

    static removeWords(line, wordsToRemove) {
        let trimmedLine = line;
        let words = this.findWords(line);
        for (let i = 0; i < wordsToRemove; i++) {
            trimmedLine = trimmedLine.trim().substr(words[i].length);
        }
        return trimmedLine.trim();
    }
    /**
     * Find all words in a line and concatenate all words with single spaces between.
     * @param line {String}
     * @returns {String[]}
     */
    static findWords(line) {
        return line.split(' ').filter((s) => !!s);
    }

    /**
     * Find all word groups within a line, with maximum expected spacing between words in a group.
     * @param line {String}
     * @param maxSpaceLength {Number}
     * @returns {String[]}
     */
    static findWordGroups(line, maxSpaceLength) {
        let splitString = '';
        for (let i = 0; i < maxSpaceLength; i++) splitString += ' ';
        return line
            .split(splitString)
            .filter(Boolean)
            .map((s) => s.trim());
    }

    /**
     * Check if words start at the same position in two lines.
     * @param line1 {String}
     * @param line2 {String}
     * @param wordsToCheck {Number}
     * @returns {boolean}
     */
    static isWordPositionMatching(line1, line2, wordsToCheck = -1) {
        let line1Words = this.findWords(line1);
        let line2Words = this.findWords(line2);
        let minLength = Math.min(line1Words.length, line2Words.length);
        let length = wordsToCheck === -1 || minLength < wordsToCheck ? minLength : wordsToCheck;
        let position = 0;

        if (length === 0) return false;
        for (let i = 0; i < length; i++) {
            // find index of first char that is not a space
            while (
                position < line1.length &&
                position < line2.length &&
                line1[position] === ' ' &&
                line2[position] === ' '
            )
                position++;
            if (position === line1.length || position === line2.length) return true;
            else if (line1[position] !== ' ' && line2[position] !== ' ') continue;
            else return false;
        }
        return true;
    }
}

module.exports = searchablePdfHelper;
