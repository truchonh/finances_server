/**
 * Created by Hugo on 2018-11-30
 */
import * as _ from 'lodash';

export class TreeNode<TElement> {
    private readonly tree: NTree<TElement>;
    public value?: TElement;
    public parent?: TreeNode<TElement>;
    public children?: Array<TreeNode<TElement>>;

    constructor(
        treeInstance: NTree<TElement>,
        {
            value,
            parent,
            children = [],
        }: { value?: TElement; parent?: TreeNode<TElement>; children?: Array<TreeNode<TElement>> }
    ) {
        this.tree = treeInstance;
        this.value = value;
        this.parent = parent;
        this.children = children;
    }

    get isLeaf() {
        return _.isEmpty(this.children);
    }

    getLeaves(): Array<TElement> {
        const search = (node: TreeNode<TElement>): Array<TreeNode<TElement>> => {
            if (node.isLeaf) {
                return [node];
            }
            return _.flatten(node.children.map((child) => search(child)));
        };
        return search(this).map((leaf) => leaf.value);
    }

    toArray() {
        const search = (node: TreeNode<TElement>): Array<TreeNode<TElement>> => {
            return [node, ..._.flatten(node.children.map((child) => search(child)))];
        };
        return search(this).map((leaf) => leaf.value);
    }

    getNode(key: string) {
        return this.tree.getNode(key);
    }
}

export default class NTree<TElement> {
    private readonly keySelector: (element: TElement) => string;
    private readonly parentSelector: (element: TElement) => string;
    private readonly nodeMap: Map<string, TreeNode<TElement>>;
    private readonly rootNodes: Array<TreeNode<TElement>>;

    constructor(
        elements: Array<TElement>,
        keySelector: (element: TElement) => string,
        parentSelector: (element: TElement) => string
    ) {
        if (typeof keySelector !== 'function') throw new Error('Second parameter keySelector must be a function.');
        if (typeof parentSelector !== 'function') throw new Error('Third parameter parentSelector must be a function.');

        this.keySelector = keySelector;
        this.parentSelector = parentSelector;
        this.nodeMap = new Map();
        this.rootNodes = [];

        this.addMany(elements);
    }

    addMany(elements: Array<TElement>) {
        for (let element of elements) {
            this.add(element);
        }
    }

    add(element: TElement) {
        let key = this.keySelector(element);
        let parent = this.parentSelector(element);

        // if the node was created earlier (line 54), an empty node for this object will exists.
        let thisNode = this.nodeMap.get(key);

        if (thisNode === undefined) {
            thisNode = new TreeNode(this, { value: element });
            this.nodeMap.set(key, thisNode);
        } else {
            thisNode.value = element;
        }

        if (!parent) {
            this.rootNodes.push(thisNode);
        } else {
            let parentNode = this.nodeMap.get(parent);

            if (parentNode === undefined) {
                // pre-create the parent node if its not in the tree yet.
                parentNode = new TreeNode(this, {});
                this.nodeMap.set(parent, parentNode);
            }

            parentNode.children.push(thisNode);
            thisNode.parent = parentNode;
        }
    }

    getRootNodes() {
        return this.rootNodes;
    }

    getNode(key: string) {
        return this.nodeMap.get(key);
    }

    findNode(searchFn: (node: TreeNode<TElement>) => boolean): TreeNode<TElement> | null {
        const nodes = [...this.nodeMap.values()];
        return nodes.find(searchFn);
    }

    mapLeafsToRoots<T>(propertySelector: (element: TElement) => T) {
        let leafMap: Map<T, T> = new Map();
        for (let rootNode of this.rootNodes) {
            let children = [...rootNode.children];
            let index = 0;

            if (_.isEmpty(children)) {
                // If the node has no parent and no children, it is mapped to itself.
                leafMap.set(propertySelector(rootNode.value), propertySelector(rootNode.value));
            }

            while (index < children.length) {
                let childNode = children[index];
                if (childNode.isLeaf) {
                    leafMap.set(propertySelector(childNode.value), propertySelector(rootNode.value));
                } else {
                    childNode.children.forEach((_c) => children.push(_c));
                }
                index++;
            }
        }
        return leafMap;
    }

    /**
     * Map each leaf of the tree(s) to its hierarchy of nodes.<br/>
     * The hierarchy array for each leaf will be composed of the value selected with the `propertySelector`.
     */
    getCategoryHierarchy<T>(propertySelector: (element: TElement) => T) {
        const hierarchyMap: Map<string, Array<T>> = new Map();

        const mapChildren = (node: TreeNode<TElement>, hierarchy: Array<T>) => {
            if (_.isEmpty(node.children)) {
                hierarchyMap.set(this.keySelector(node.value), [...hierarchy]);
            }
            for (let child of node.children) {
                mapChildren(child, [...hierarchy, propertySelector(child.value)]);
            }
        };

        for (let root of this.rootNodes) {
            mapChildren(root, [propertySelector(root.value)]);
        }
        return hierarchyMap;
    }

    iterate(predicate: (node: TreeNode<TElement>) => void) {
        this.iterateChild(this.rootNodes, predicate);
    }

    private iterateChild(nodes: Array<TreeNode<TElement>>, predicate: (node: TreeNode<TElement>) => void) {
        for (let node of nodes) {
            predicate(node);
            this.iterateChild(node.children, predicate);
        }
    }

    iterateConditional(predicate: (node: TreeNode<TElement>) => boolean) {
        this.iterateChildConditional(this.rootNodes, predicate);
    }

    private iterateChildConditional(
        nodes: Array<TreeNode<TElement>>,
        predicate: (node: TreeNode<TElement>) => boolean | void
    ) {
        for (let node of nodes) {
            const res = predicate(node);
            if (res) {
                this.iterateChildConditional(node.children, predicate);
            }
        }
    }
}
