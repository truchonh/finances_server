import { Express } from 'express';
import * as Sentry from '@sentry/node';
import * as morgan from 'morgan';
import * as dayjs from 'dayjs';
import * as fs from 'fs';
import * as path from 'path';
import paths from '../../config/paths';

type SentryConfig = {
    app: Express;
    sampleRate: number;
    environment: string;
};
export function initSentry({ app, sampleRate, environment }: SentryConfig) {
    Sentry.init({
        dsn: 'https://ea2272c9e241b7f7fd05dc8d7054a093@o4506641804886016.ingest.sentry.io/4506641959419904',
        environment: environment,
        integrations: [
            new Sentry.Integrations.Http({ tracing: true }),
            new Sentry.Integrations.Express({ app }),
            new Sentry.Integrations.Mongo({ useMongoose: true }),
        ],
        tracesSampleRate: sampleRate,
        profilesSampleRate: 1.0,
        beforeSendTransaction(event) {
            if (event.request?.data) {
                delete event.request.data;
            }
            return event;
        },
    });
}

export function initApiLogger(app: Express) {
    const dateFormat: any = {
        year: 'numeric',
        month: '2-digit',
        day: '2-digit',
        hour: '2-digit',
        minute: '2-digit',
        second: '2-digit',
        timeZoneName: 'short',
    };
    morgan.token('local-date', function () {
        return new Date().toLocaleString('fr-CA', dateFormat);
    });

    // log request in a seperate file
    const fsCompatibleDate = dayjs().format('YYYY-MM-DD[_]HH.mm.ss.SSS[_]ZZ');
    let accessLogStream = fs.createWriteStream(path.join(paths.SYSTEM_LOGS, `${fsCompatibleDate}__api.txt`), {
        flags: 'a',
    });
    app.use(
        morgan(':local-date :method :status :url :response-time ms - :res[content-length]', {
            stream: accessLogStream,
        })
    );
}
